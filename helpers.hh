#ifndef HELPERS_HH
#define HELPERS_HH
std::ofstream datei("mat.txt");
#include <dune/istl/io.hh>
template <typename Mat>
void print_to_file(Mat A, std::string name)
{
     //Dune::printmatrix(datei, A, name, "", 24, 4);
}
//Helper function to access V by 2D coordinates
//arguments:
// vec
// x coord
// y coord
//upper oberes dreieck?
// i 0,1,2

int indexInArr(int x, int y, bool upper, int i, int width)
{
    //std::cout<<"x:"<<x<<" y:"<<y<<"upper"<<upper<<" i:"<<i<<" w:"<<width<<std::endl;
    if (!upper)
    {
        switch (i)
        {
        case 0:
            /* code */
            break;
        case 2:
            y += 1;//c++ also run next case

            x += 1;
            break;
        case 1:
            x += 1;
            /* code */
            break;
        default:
            break;
        }
    }
    else
    {
        switch (i)
        {
        case 0:
            /* code */
            y+=1;
            break;
        case 2:
            y += 1;
            x += 1;
            break;
        case 1:
            /* code */
            break;
        default:
            break;
        }
    }
    int ind = (y * width + x);

    //std::cerr<<x<<","<<y<<"="<<ind<<std::endl;
    if (ind >= width * width)
    {
        std::cerr <<"Fehler"<< ind << " " << x << " " << y << " " << upper << " " << i << " " << width << std::endl;
    } // else {std::cout<<ind<<std::endl;}
    return ind;
}
template <typename V, typename ctype>
ctype &accessV2D(V &vec, size_t x, size_t y, bool upper, size_t i, size_t width)
{
    return vec[indexInArr(x, y, upper, i, width)];
}
template <typename V, typename ctype>
ctype accessV2Dc(const V &vec, int x, int y, bool upper, int i, int width)
{
     return vec[indexInArr(x, y, upper, i, width)];
}
//Restriction matrix builder on 1*1 field with P_1 elements.
// from finer to coarser grid
// 2^j to 2^(j-1)
template <typename CGM>
CGM restrictionMatrix(size_t j)
{
    const size_t width_coarser = pow(2, j - 1) + 1;
    const size_t width_finer = pow(2, j) + 1;
    CGM R(width_coarser * width_coarser, width_finer * width_finer, 0); //each triangle has 3 basis functions

    std::cout << "Finer:" << width_coarser << std::endl;
    std::cout << "Coarser:" << width_finer << std::endl;
    //now go through coarser grid and set values where needed
    for (size_t k = 0; k < width_coarser - 1; k++)
    {
        for (size_t l = 0; l < width_coarser - 1; l++)
        {
            //Lower right triangle
            // kronecker_1
            //in triangle 0
            R[indexInArr(k, l, false, 1, width_coarser)][indexInArr(k * 2 + 1, l * 2, false, 1, width_finer)] = 1.0; //normal ascent
            R[indexInArr(k, l, false, 1, width_coarser)][indexInArr(k * 2 + 1, l * 2, false, 0, width_finer)] = 0.5;
            R[indexInArr(k, l, false, 1, width_coarser)][indexInArr(k * 2 + 1, l * 2, false, 2, width_finer)] = 0.5;
            //triangle one asscent 0.5 on 1 and 2
            R[indexInArr(k, l, false, 1, width_coarser)][indexInArr(k * 2 + 1, l * 2 + 1, false, 1, width_finer)] = 0.5;
            R[indexInArr(k, l, false, 1, width_coarser)][indexInArr(k * 2, l * 2, false, 1, width_finer)] = 0.5;
            // in triangle 2 and 4 same situation: only kronecker_0 needs to be touched
            R[indexInArr(k, l, false, 1, width_coarser)][indexInArr(k * 2 + 1, l * 2, true, 2, width_finer)] = 0.5;
            R[indexInArr(k, l, false, 1, width_coarser)][indexInArr(k * 2 + 1, l * 2, true, 1, width_finer)] = 0.5; //triangle 4 is one row higher
            // case kronecker_0
            R[indexInArr(k, l, false, 0, width_coarser)][indexInArr(k * 2, l * 2, false, 0, width_finer)] = 1;
            R[indexInArr(k, l, false, 0, width_coarser)][indexInArr(k * 2, l * 2, false, 1, width_finer)] = 0.5;
            R[indexInArr(k, l, false, 0, width_coarser)][indexInArr(k * 2, l * 2, false, 2, width_finer)] = 0.5;

            R[indexInArr(k, l, false, 0, width_coarser)][indexInArr(k * 2 + 1, l * 2, false, 0, width_finer)] = 0.5;
            R[indexInArr(k, l, false, 0, width_coarser)][indexInArr(k * 2 + 1, l * 2 + 1, false, 0, width_finer)] = 0.5;

            R[indexInArr(k, l, false, 0, width_coarser)][indexInArr(k * 2 + 1, l * 2, true, 0, width_finer)] = 0.5;
            R[indexInArr(k, l, false, 0, width_coarser)][indexInArr(k * 2 + 1, l * 2, true, 1, width_finer)] = 0.5;
            //kronecker_2 are somewhat symmetric
            R[indexInArr(k, l, false, 2, width_coarser)][indexInArr(k * 2 + 1, l * 2 + 1, false, 2, width_finer)] = 1;
            R[indexInArr(k, l, false, 2, width_coarser)][indexInArr(k * 2 + 1, l * 2 + 1, false, 1, width_finer)] = 0.5;
            R[indexInArr(k, l, false, 2, width_coarser)][indexInArr(k * 2 + 1, l * 2 + 1, false, 0, width_finer)] = 0.5;

            R[indexInArr(k, l, false, 2, width_coarser)][indexInArr(k * 2 + 1, l * 2, false, 2, width_finer)] = 0.5;
            R[indexInArr(k, l, false, 2, width_coarser)][indexInArr(k * 2, l * 2, false, 2, width_finer)] = 0.5;

            R[indexInArr(k, l, false, 2, width_coarser)][indexInArr(k * 2 + 1, l * 2, true, 0, width_finer)] = 0.5;
            R[indexInArr(k, l, false, 2, width_coarser)][indexInArr(k * 2 + 1, l * 2, true, 2, width_finer)] = 0.5;

            // upper
            R[indexInArr(k, l, true, 1, width_coarser)][indexInArr(k * 2, l * 2, true, 1, width_finer)] = 1.0; //normal ascent
            R[indexInArr(k, l, true, 1, width_coarser)][indexInArr(k * 2, l * 2, true, 0, width_finer)] = 0.5;
            R[indexInArr(k, l, true, 1, width_coarser)][indexInArr(k * 2, l * 2, true, 2, width_finer)] = 0.5;
            //triangle one asscent 0.5 on 1 and 2
            R[indexInArr(k, l, true, 1, width_coarser)][indexInArr(k * 2 + 1, l * 2 + 1, true, 1, width_finer)] = 0.5;
            R[indexInArr(k, l, true, 1, width_coarser)][indexInArr(k * 2, l * 2 + 1, true, 1, width_finer)] = 0.5;

            // in triangle 2 and 4 same situation: only kronecker_0 needs to be touched
            R[indexInArr(k, l, true, 1, width_coarser)][indexInArr(k * 2, l * 2 + 1, false, 0, width_finer)] = 0.5;
            R[indexInArr(k, l, true, 1, width_coarser)][indexInArr(k * 2, l * 2 + 1, false, 1, width_finer)] = 0.5; //triangle 4 is one row higher
            // case kronecker_0
            R[indexInArr(k, l, true, 0, width_coarser)][indexInArr(k * 2, l * 2 + 1, true, 0, width_finer)] = 1;
            R[indexInArr(k, l, true, 0, width_coarser)][indexInArr(k * 2, l * 2 + 1, true, 1, width_finer)] = 0.5;
            R[indexInArr(k, l, true, 0, width_coarser)][indexInArr(k * 2, l * 2 + 1, true, 2, width_finer)] = 0.5;

            R[indexInArr(k, l, true, 0, width_coarser)][indexInArr(k * 2, l * 2, true, 0, width_finer)] = 0.5;
            R[indexInArr(k, l, true, 0, width_coarser)][indexInArr(k * 2 + 1, l * 2 + 1, true, 0, width_finer)] = 0.5;

            R[indexInArr(k, l, true, 0, width_coarser)][indexInArr(k * 2, l * 2 + 1, false, 0, width_finer)] = 0.5;
            R[indexInArr(k, l, true, 0, width_coarser)][indexInArr(k * 2, l * 2 + 1, false, 2, width_finer)] = 0.5;
            //kronecker_2 are somewhat symmetric
            R[indexInArr(k, l, true, 2, width_coarser)][indexInArr(k * 2 + 1, l * 2 + 1, true, 2, width_finer)] = 1;
            R[indexInArr(k, l, true, 2, width_coarser)][indexInArr(k * 2 + 1, l * 2 + 1, true, 1, width_finer)] = 0.5;
            R[indexInArr(k, l, true, 2, width_coarser)][indexInArr(k * 2 + 1, l * 2 + 1, true, 0, width_finer)] = 0.5;

            R[indexInArr(k, l, true, 2, width_coarser)][indexInArr(k * 2, l * 2 + 1, true, 2, width_finer)] = 0.5;
            R[indexInArr(k, l, true, 2, width_coarser)][indexInArr(k * 2, l * 2, true, 2, width_finer)] = 0.5;

            R[indexInArr(k, l, true, 2, width_coarser)][indexInArr(k * 2, l * 2 + 1, false, 1, width_finer)] = 0.5;
            R[indexInArr(k, l, true, 2, width_coarser)][indexInArr(k * 2, l * 2 + 1, false, 2, width_finer)] = 0.5;
        }
    }

    //TODO: Multiply by constant which describes the ascent
    //R*=pow(0.5,((double)j)+0.25)*3.0;
    return R;
}
#endif