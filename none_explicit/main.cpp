//This follows the tutorial by Oliver Sander (see Introduction to DUNE)
#include "config.h"
#include <dune/common/parallel/mpihelper.hh>

#define HAVE_UG 1
#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/matrix.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/yaspgrid.hh>
#include "multigrid.hh"
#include "helpers.hh"
#include <dune/functions/functionspacebases/boundarydofs.hh>
#include <dune/istl/umfpack.hh>
#include <dune/common/parametertreeparser.hh>
#include "loaders.hh"
typedef Dune::BlockVector<Dune::FieldVector<double, 1>> VectorType;
typedef Dune::BCRSMatrix<Dune::FieldMatrix<double, 1, 1>> MatrixType;
template <typename NM, typename NVT>
void directSolve(NM &A_j, NVT &xk_j, NVT b_j)
{
    typedef Dune::MatrixAdapter<NM, NVT, NVT> LinearOperator;
    Dune::InverseOperatorResult result;
    /*LinearOperator op(native(A_j));
            typedef Dune::SeqILU<NM, NVT, NVT> Preconditioner;
            Preconditioner prec(native(A_j), 1.8);

            const double reduction = 1e-9;
            const int max_iterations = 5000;
            const int verbose = 1;

            typedef Dune::CGSolver<NVT> Solver;
            Solver solver(op, prec, reduction, max_iterations, verbose);
            solver.apply(native(xk_j), native(b_j), result);*/
    Dune::UMFPack<NM> umf(A_j, 0);
    umf.apply(xk_j, b_j, result); //TODO(henrik):Warning
}
template <class Basis>
void getOccupationPattern(const Basis &basis, Dune::MatrixIndexSet &nb)
{
    nb.resize(basis.size(), basis.size());
    auto gridView = basis.gridView();
    auto localView = basis.localView();
    //auto localIndexSet = basis.makeIndexSet();
    for (const auto &element : Dune::elements(gridView))
    {
        localView.bind(element);
        for (size_t i = 0; i < localView.size(); i++)
        {
            auto row = localView.index(i);
            for (size_t j = 0; j < localView.size(); j++)
            {
                auto col = localView.index(j);
                nb.add(row, col);
            }
        }
    }
}
template <class LocalView, class MType>
void assembleElementMatrix(const LocalView &localView, MType &elementMatrix, MType &elementMassMatrix, double h)
{
    using Element = typename LocalView::Element;
    auto element = localView.element();
    const int dim = Element::dimension;
    auto geometry = element.geometry();
    // Get set of shape functions for this element
    const auto &localFiniteElement = localView.tree().finiteElement();
    elementMatrix.setSize(localFiniteElement.size(), localFiniteElement.size());
    elementMassMatrix.setSize(localFiniteElement.size(), localFiniteElement.size());
    //init
    elementMatrix = 0;
    elementMassMatrix = 0;
    //calc order
    int order = 2 * (dim * localFiniteElement.localBasis().order() - 1);
    const auto &quadRule = Dune::QuadratureRules<double, dim>::rule(element.type(), order);
    for (const auto &quadPoint : quadRule)
    {
        // Position of the current quadrature point in the reference element
        const auto quadPos = quadPoint.position();
        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto jacobian = geometry.jacobianInverseTransposed(quadPos);
        const auto jacobianNon = geometry.jacobianTransposed(quadPos);
        // The multiplicative factor in the integral transformation formula
        const auto integrationElement = geometry.integrationElement(quadPos);
        // The gradients of the shape functions on the reference element
        std ::vector<Dune::FieldMatrix<double, 1, dim>> referenceGradients;
        std ::vector<Dune::FieldVector<double, 1>> values;
        localFiniteElement.localBasis().evaluateJacobian(quadPos, referenceGradients);
        localFiniteElement.localBasis().evaluateFunction(quadPos, values);
        // Compute the shape function gradients on the real element
        std ::vector<Dune::FieldVector<double, dim>> gradients(referenceGradients.size());
        for (size_t i = 0; i < gradients.size(); i++)
            jacobian.mv(referenceGradients[i][0], gradients[i]);
        std::cout << integrationElement << std::endl;
        std::cout << "size:" << values.size() << std::endl;
        for (const auto &val : values)
            std::cout << val << std::endl;
        std::cout << "quad:" << quadPoint.weight() << std::endl;
        for (size_t i = 0; i < elementMatrix.N(); i++)
        {
            for (size_t j = 0; j < elementMatrix.M(); j++)
            {
                //Added second part for matrix
                elementMatrix[localView.tree().localIndex(i)][localView.tree().localIndex(j)] += ((gradients[i] * gradients[j]) * quadPoint.weight() * integrationElement) + values[i] * values[j] * quadPoint.weight() * integrationElement * 1.0 / h;
                //Added second part for matrix
                elementMassMatrix[localView.tree().localIndex(i)][localView.tree().localIndex(j)] += values[i] * values[j] * quadPoint.weight() * integrationElement * 1.0 / h;
            }
        }
    }
}
// Compute the heatsource term for a single element
template <class LocalView>
void getStartHeat(const LocalView &localView,
                  Dune::BlockVector<Dune::FieldVector<double, 1>> &localRhs,
                  const std::function<double(Dune::FieldVector<double, LocalView::Element::dimension>)> heatTerm)
{
    using Element = typename LocalView::Element;
    auto element = localView.element();
    const int dim = Element::dimension;
    // Set of shape functions for a single element
    const auto &localFiniteElement = localView.tree().finiteElement();
    // Set all entries to zero
    localRhs.resize(localFiniteElement.size());
    localRhs = 0;
    // A quadrature rule
    int order = dim;
    const auto &quadRule = Dune::QuadratureRules<double, dim>::rule(element.type(), order);
    // Loop over all quadrature points
    for (const auto &quadPoint : quadRule)
    {
        // Position of the current quadrature point in the reference element
        const Dune::FieldVector<double, dim> &quadPos = quadPoint.position();
        // The multiplicativefactor in the integraltransformation formula
        const double integrationElement = element.geometry().integrationElement(quadPos);
        double functionValue = heatTerm(element.geometry().global(quadPos));
        // Evaluate all shape function values at this point
        std ::vector<Dune::FieldVector<double, 1>> shapeFunctionValues;
        localFiniteElement.localBasis().evaluateFunction(quadPos, shapeFunctionValues);
        // Actually compute the vector entries
        for (size_t i = 0; i < localRhs.size(); i++)
            localRhs[i] += shapeFunctionValues[i] * functionValue * quadPoint.weight();
    }
}
template <class Basis>
void assembleHeatEquationProblemForMethodOfLinesWithoutInvert(const Basis &basis,
                                                              MatrixType &matrix,
                                                              MatrixType &massMatrix,
                                                              VectorType &rhs,
                                                              const std::function<double(Dune::FieldVector<double, Basis::GridView::dimension>)> heatTerm,
                                                              double h)
{
    auto gridView = basis.gridView();
    //get Occupation pattern
    Dune::MatrixIndexSet occupationPattern;
    getOccupationPattern(basis, occupationPattern);
    //and apply it so we can access the entries
    occupationPattern.exportIdx(matrix);
    occupationPattern.exportIdx(massMatrix);
    matrix = 0;
    massMatrix = 0;
    // set rhs to correct length
    rhs.resize(basis.size());
    //set all rhs to zero
    rhs = 0;
    auto localView = basis.localView();
    for (const auto &element : Dune::elements(gridView))
    {
        localView.bind(element);
        Dune::Matrix<Dune::FieldMatrix<double, 1, 1>> elementMatrix, elementMassMatrix;
        assembleElementMatrix(localView, elementMatrix, elementMassMatrix, h);
        for (size_t i = 0; i < elementMatrix.N(); i++)
        {
            auto row = localView.index(i);
            for (size_t j = 0; j < elementMatrix.M(); j++)
            {
                auto col = localView.index(j);
                matrix[row][col] += elementMatrix[i][j];
                massMatrix[row][col] += elementMassMatrix[i][j];
            }
        }
        // Now get the localcontribution to the right −hand side vector
        Dune::BlockVector<Dune::FieldVector<double, 1>> localRhs;
        getStartHeat(localView, localRhs, heatTerm);
        for (size_t i = 0; i < localRhs.size(); i++)
        {
            // The global index of the i −th vertex
            auto row = localView.index(i);
            rhs[row] += localRhs[i];
        }
    }
}
int main(int argc, char **argv)
{
    Dune::MPIHelper::instance(argc, argv);
    try
    {
        Dune::ParameterTree pt;
        Dune::ParameterTreeParser::readOptions(argc,argv,pt);
        const int dim = 2;
        typedef Dune::UGGrid<dim> GridType;
        typedef Dune::FieldVector<double, 2> V2;
        V2 ll, ur;
        ur[1] = 1;
        ur[0] = 1;
        std::array<unsigned, 2> ar;
        const size_t W = 2;
        ar[0] = W;
        ar[1] = W;
        auto grid = Dune::StructuredGridFactory<GridType>::createSimplexGrid(ll, ur, ar);
        //std ::shared_ptr<GridType> grid(Dune::GmshReader<GridType>::read("l-shape.msh"));
        grid->globalRefine(pt.get<int>("refine",3));
        typedef GridType::LeafGridView GridView;
        GridView gridView = grid->leafGridView();
        Dune::Functions::LagrangeBasis<GridView, 1> basis(gridView);
        auto sourceTerm = [](const Dune::FieldVector<double, dim> &x) {
            if (std::abs(x[0] - 0.5) < 0.25 && std::abs(x[1] - 0.5) < 0.25)
                return 1.0;
            return 0.0;
        };
        VectorType rhs;
        MatrixType matrix, massMatrix;
        assembleHeatEquationProblemForMethodOfLinesWithoutInvert(basis, matrix, massMatrix, rhs, sourceTerm, 1.0/((double)pt.get<int>("timestepcount",256)));
        // Mark all dirichletNodes
        std ::vector<char> dirichletNodes(basis.size(), false);
        //boundary cdt taken from https://github.com/dune-project/dune-functions/blob/master/examples/poisson-pq2.cc
        Dune::Functions::forEachBoundaryDOF(basis, [&](auto &&index) {
            dirichletNodes[index] = true;
        });
        for (size_t i = 0; i < matrix.N(); i++)
        {
            if (dirichletNodes[i])
            {
                rhs[i] = 0.0; //rhs should have zero bd condition
                {
                    auto cIt = matrix[i].begin();
                    auto cEndIt = matrix[i].end();
                    // loop over nonzero matrix entries in current row
                    for (; cIt != cEndIt; ++cIt)
                        *cIt = (i == cIt.index()) ? 1 : 0;
                }
                {
                    auto cIt = massMatrix[i].begin();
                    auto cEndIt = massMatrix[i].end();
                    // loop over nonzero matrix entries in current row
                    for (; cIt != cEndIt; ++cIt)
                        *cIt = (i == cIt.index()) ? 1 : 0;
                }
            }
            else
            {
                {
                    auto cIt = matrix[i].begin();
                    auto cEndIt = matrix[i].end();
                    // loop over nonzero matrix entries in current row
                    for (; cIt != cEndIt; ++cIt)
                        if (dirichletNodes[cIt.index()])
                            *cIt = 0;
                }
                {
                    auto cIt = massMatrix[i].begin();
                    auto cEndIt = massMatrix[i].end();
                    // loop over nonzero matrix entries in current row
                    for (; cIt != cEndIt; ++cIt)
                        if (dirichletNodes[cIt.index()])
                            *cIt = 0;
                }
            }
        }
        //test direct solve
        const size_t count = pt.get<int>("timestepcount",256);
        auto cEV = [](auto k, size_t j) {
            std::cerr << "Shouldn't be called by direct solve" << std::endl;
            // exit(1);
            return 0.0;
        };
        VectorType u_0;
        u_0.resize(rhs.size());
        auto A_j = [&](size_t j) {
            return matrix;
        };
        typedef Dune::DynamicMatrix<double> CGM;
        std::vector<VectorType> resultsOverTime;
        resultsOverTime.push_back(rhs);
        VectorType total;
        total.resize(rhs.size()*count);
        std::cout<<"Total:"<<total.size()<<std::endl;
        for (size_t i = 0; i < count; i++)
        {
            /* direct solve */
            auto b_j2 = resultsOverTime.at(i);
            auto b_j = b_j2;
            massMatrix.mv(b_j2, b_j);
            VectorType nextStep;
            nextStep.resize(basis.size());
            directSolve(matrix,nextStep,b_j);
            //auto nextStep = MGM<VectorType, VectorType, MatrixType, CGM, double>(1, u_0, b_j, A_j, &restrictionMatrix<CGM>, cEV, 1, 1, 1);
            resultsOverTime.push_back(nextStep);
            Dune::VTKWriter<GridView> vtkWriter(gridView);
            vtkWriter.addVertexData(b_j2, "solution");
            std::stringstream str;
            str << i;
            vtkWriter.write("result_" + str.str());
            for(int j =0;j<rhs.size();j++){
                total[rhs.size()*i+j]= b_j2[j];
            }
        }
        writeToFile(total,"total.test");
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
    }
}