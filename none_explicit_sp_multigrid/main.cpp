//This follows the tutorial by Oliver Sander (see Introduction to DUNE)
#include "config.h"
#include <dune/common/parallel/mpihelper.hh>

#define HAVE_UG 1
#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/matrix.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/yaspgrid.hh>
#include "space_time_mgrid_driver.hh"
#include "helpers.hh"
#include <dune/functions/functionspacebases/boundarydofs.hh>
#include "prolongation.hh"
#include <dune/istl/umfpack.hh>
#include <dune/istl/preconditioners.hh>
#include "smoothers.hh"
#include "loaders.hh"
#include <dune/common/parametertreeparser.hh>
typedef Dune::BlockVector<Dune::FieldVector<double, 1>> VectorType;
typedef Dune::BCRSMatrix<Dune::FieldMatrix<double, 1, 1>> MatrixType;

template <typename NM, typename NVT>
void directSolve(NM &A_j, NVT &xk_j, NVT b_j)
{
    typedef Dune::MatrixAdapter<NM, NVT, NVT> LinearOperator;
    Dune::InverseOperatorResult result;
    /*LinearOperator op(native(A_j));
            typedef Dune::SeqILU<NM, NVT, NVT> Preconditioner;
            Preconditioner prec(native(A_j), 1.8);

            const double reduction = 1e-9;
            const int max_iterations = 5000;
            const int verbose = 1;

            typedef Dune::CGSolver<NVT> Solver;
            Solver solver(op, prec, reduction, max_iterations, verbose);
            solver.apply(native(xk_j), native(b_j), result);*/
    Dune::UMFPack<NM> umf(A_j, 2);
    umf.apply(xk_j, b_j, result); //TODO(henrik):Warning
}

template <typename NM, typename NVT>
void CGSolve(NM &A_j, NVT &xk_j, NVT b_j)
{
    typedef Dune::MatrixAdapter<NM, NVT, NVT> LinearOperator;
    Dune::InverseOperatorResult result;
    LinearOperator op(A_j);
    typedef Dune::SeqILU<NM, NVT, NVT> Preconditioner;
    Preconditioner prec(A_j, 1.8);

    const double reduction = 1e-9;
    const int max_iterations = 5000;
    const int verbose = 1;

    typedef Dune::CGSolver<NVT> Solver;
    Solver solver(op, prec, reduction, max_iterations, verbose);
    solver.apply(xk_j, b_j, result);
    //Dune::UMFPack<NM> umf(A_j, 2);
    //umf.apply(xk_j, b_j, result); //TODO(henrik):Warning
}
template <class Basis>
void getOccupationPattern(const Basis &basis, Dune::MatrixIndexSet &nb,Dune::MatrixIndexSet &small, size_t nOfTimesteps)
{
    nb.resize(basis.size() * nOfTimesteps, basis.size() * nOfTimesteps);
    small.resize(basis.size() , basis.size() );
    auto gridView = basis.gridView();
    auto localView = basis.localView();
    //auto localIndexSet = basis.makeIndexSet();
    for (const auto &element : Dune::elements(gridView))
    {
        localView.bind(element);
        for (size_t i = 0; i < localView.size(); i++)
        {
            auto row = localView.index(i);
            for (size_t j = 0; j < localView.size(); j++)
            {
                auto col = localView.index(j);
                small.add(  row, col);
                for (size_t k = 0; k < nOfTimesteps; k++)
                {
                    nb.add(k * basis.size() + row, k * basis.size() + col);
                    //add b if possible
                    if (k > 0)
                    {
                        nb.add((k)*basis.size() + row, (k - 1) * basis.size() + col);
                    }
                }
            }
        }
    }
    for (int i = 0; i < basis.size(); i++)
    {
        nb.add(i, i); //TODO(henrik): Reset that and multiply result in getStartHeat
    }
}
template <class LocalView, class MType>
void assembleElementMatrix(const LocalView &localView, MType &elementMatrix, MType &elementMassMatrix)
{
    using Element = typename LocalView::Element;
    auto element = localView.element();
    const int dim = Element::dimension;
    auto geometry = element.geometry();
    // Get set of shape functions for this element
    const auto &localFiniteElement = localView.tree().finiteElement();
    elementMatrix.setSize(localFiniteElement.size(), localFiniteElement.size());
    elementMassMatrix.setSize(localFiniteElement.size(), localFiniteElement.size());
    //init
    elementMatrix = 0;
    elementMassMatrix = 0;
    //calc order
    int order = 2 * (dim * localFiniteElement.localBasis().order() - 1);
    const auto &quadRule = Dune::QuadratureRules<double, dim>::rule(element.type(), order);
    for (const auto &quadPoint : quadRule)
    {
        // Position of the current quadrature point in the reference element
        const auto quadPos = quadPoint.position();
        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto jacobian = geometry.jacobianInverseTransposed(quadPos);
        const auto jacobianNon = geometry.jacobianTransposed(quadPos);
        // The multiplicative factor in the integral transformation formula
        const auto integrationElement = geometry.integrationElement(quadPos);
        // The gradients of the shape functions on the reference element
        std ::vector<Dune::FieldMatrix<double, 1, dim>> referenceGradients;
        std ::vector<Dune::FieldVector<double, 1>> values;
        localFiniteElement.localBasis().evaluateJacobian(quadPos, referenceGradients);
        localFiniteElement.localBasis().evaluateFunction(quadPos, values);
        // Compute the shape function gradients on the real element
        std ::vector<Dune::FieldVector<double, dim>> gradients(referenceGradients.size());
        for (size_t i = 0; i < gradients.size(); i++)
            jacobian.mv(referenceGradients[i][0], gradients[i]);
        /*std::cout << integrationElement << std::endl;
        std::cout << "size:" << values.size() << std::endl;
        for (const auto &val : values)
            std::cout << val << std::endl;
       // std::cout << "quad:" << quadPoint.weight() << std::endl;*/
        for (size_t i = 0; i < elementMatrix.N(); i++)
        {
            for (size_t j = 0; j < elementMatrix.M(); j++)
            {
                //normally we would now add code for kroneckerproduct.

                //Added second part for matrix
                elementMatrix[localView.tree().localIndex(i)][localView.tree().localIndex(j)] += ((gradients[i] * gradients[j]) * quadPoint.weight() * integrationElement) + values[i] * values[j] * quadPoint.weight() * integrationElement;
                //Added second part for matrix
                elementMassMatrix[localView.tree().localIndex(i)][localView.tree().localIndex(j)] += values[i] * values[j] * quadPoint.weight() * integrationElement;
            }
        }
    }
}
// Compute the heatsource term for a single element
template <class LocalView>
void getStartHeat(const LocalView &localView,
                  Dune::BlockVector<Dune::FieldVector<double, 1>> &localRhs,
                  const std::function<double(Dune::FieldVector<double, LocalView::Element::dimension>)> heatTerm)
{
    using Element = typename LocalView::Element;
    auto element = localView.element();
    const int dim = Element::dimension;
    // Set of shape functions for a single element
    const auto &localFiniteElement = localView.tree().finiteElement();
    // Set all entries to zero
    localRhs.resize(localFiniteElement.size());
    localRhs = 0;
    // A quadrature rule
    int order = dim;
    const auto &quadRule = Dune::QuadratureRules<double, dim>::rule(element.type(), order);
    // Loop over all quadrature points
    for (const auto &quadPoint : quadRule)
    {
        // Position of the current quadrature point in the reference element
        const Dune::FieldVector<double, dim> &quadPos = quadPoint.position();
        // The multiplicativefactor in the integraltransformation formula
        const double integrationElement = element.geometry().integrationElement(quadPos);
        double functionValue = heatTerm(element.geometry().global(quadPos));
        // Evaluate all shape function values at this point
        std ::vector<Dune::FieldVector<double, 1>> shapeFunctionValues;
        localFiniteElement.localBasis().evaluateFunction(quadPos, shapeFunctionValues);
        // Actually compute the vector entries
        for (size_t i = 0; i < localRhs.size(); i++)
            localRhs[i] += shapeFunctionValues[i] * functionValue * quadPoint.weight();
    }
}
template <class Basis>
void assembleHeatEquationProblemForMethodOfLinesWithoutInvert(const Basis &basis,
                                                              MatrixType &matrix,
                                                              MatrixType &small,
                                                              VectorType &rhs,
                                                              const std::function<double(Dune::FieldVector<double, Basis::GridView::dimension>)> heatTerm,
                                                              const size_t noOfTimesteps,
                                                              const double timestepSize)
{
    auto gridView = basis.gridView();
    //get Occupation pattern
    Dune::MatrixIndexSet occupationPattern,smallOccupationPattern;
    getOccupationPattern(basis, occupationPattern,smallOccupationPattern, noOfTimesteps);
    //and apply it so we can access the entries
    occupationPattern.exportIdx(matrix);
    smallOccupationPattern.exportIdx(small);
    //occupationPattern.exportIdx(massMatrix);
    matrix = 0;
    small=0;
    //massMatrix = 0;
    // set rhs to correct length
    rhs.resize(basis.size() * noOfTimesteps);
    //set all rhs to zero
    rhs = 0;
    auto localView = basis.localView();
    // Mark all dirichletNodes
    std ::vector<char> dirichletNodes(basis.size(), false);
    Dune::Functions::forEachBoundaryDOF(basis, [&](auto &&index) {
        dirichletNodes[index] = true;
    });
    for (const auto &element : Dune::elements(gridView))
    {
        localView.bind(element);
        Dune::Matrix<Dune::FieldMatrix<double, 1, 1>> elementMatrix, elementMassMatrix;
        assembleElementMatrix(localView, elementMatrix, elementMassMatrix);
        for (size_t i = 0; i < elementMatrix.N(); i++)
        {
            auto row = localView.index(i);
            for (size_t j = 0; j < elementMatrix.M(); j++)
            {
                auto col = localView.index(j);
                //if ((dirichletNodes[row]==false && dirichletNodes[col]==false))
                {
                    //massTime and stiffnessTime are constant, because we are only implementing backward euler
                    const double stiffnessTime = 1; //donot know the basis :(
                        const double massTime = (timestepSize)*stiffnessTime;
                        small[ row][ col] += massTime * elementMatrix[i][j] + stiffnessTime * elementMassMatrix[i][j];
                    for (size_t k = 0; k < noOfTimesteps; k++)
                    {
                        matrix[k * basis.size() + row][k * basis.size() + col] += massTime * elementMatrix[i][j] + stiffnessTime * elementMassMatrix[i][j];
                        //adding B_tau_h if possible
                        if (k > 0)
                        {
                            //N_tau is constant, because it represents function before/ we use p_t =0
                            const double N_tau = stiffnessTime;
                            matrix[(k)*basis.size() + row][(k - 1) * basis.size() + col] += -N_tau * elementMassMatrix[i][j];
                        }
                        //massMatrix[row][col] += elementMassMatrix[i][j];
                    }
                }
            }
        }
        // Now get the localcontribution to the right −hand side vector
        Dune::BlockVector<Dune::FieldVector<double, 1>> localRhs;
        getStartHeat(localView, localRhs, heatTerm);
        for (size_t i = 0; i < localRhs.size(); i++)
        {
            // The global index of the i −th vertex
            auto row = localView.index(i);
            if (dirichletNodes[row] == false)
                rhs[row] += localRhs[i];
        }
    }
    // Mark all dirichletNodes
    //boundary cdt taken from https://github.com/dune-project/dune-functions/blob/master/examples/poisson-pq2.cc
    Dune::Functions::forEachBoundaryDOF(basis, [&](auto &&index) {
        //dirichletNodes[index] = true;
        rhs[index] = 0.0;
           {
                auto cIt = small[ + index].begin();
                    auto cEndIt = small[index].end();
                    // loop over nonzero matrix entries in current row
                    for (; cIt != cEndIt; ++cIt)
                    {
                        *cIt=(index ==(cIt.index()))?1.0:0.0;
                    }
                }
        for (size_t k = 0; k < noOfTimesteps; k++)
        {
            {
                auto cIt = matrix[k * basis.size() + index].begin();
                    auto cEndIt = matrix[k * basis.size() + index].end();
                    // loop over nonzero matrix entries in current row
                    for (; cIt != cEndIt; ++cIt)
                    {
                        *cIt=(index ==(cIt.index()-k*basis.size()))?1.0:0.0;
                    }
                }
                if(k>0){
                    auto cIt = matrix[(k-1) * basis.size() + index].begin();
                    auto cEndIt = matrix[(k-1) * basis.size() + index].end();
                    // loop over nonzero matrix entries in current row
                    for (; cIt != cEndIt; ++cIt)
                    {
                        *cIt=(index ==(cIt.index()-(k-1)*basis.size()))?1.0:0.0;
                    }
                }
        }
    });
}
int main(int argc, char **argv)
{
    Dune::MPIHelper::instance(argc, argv);
    //try
    {
        Dune::ParameterTree pt;
        Dune::ParameterTreeParser::readOptions(argc,argv,pt);
        const int dim = 2;
        typedef Dune::UGGrid<dim> GridType;
        typedef Dune::FieldVector<double, 2> V2;
        V2 ll, ur;
        ur[1] = 1;
        ur[0] = 1;
        std::array<unsigned, 2> ar;
        const size_t W = 2;
        ar[0] = W;
        ar[1] = W;
        auto grid = Dune::StructuredGridFactory<GridType>::createSimplexGrid(ll, ur, ar);
        //std ::shared_ptr<GridType> grid(Dune::GmshReader<GridType>::read("l-shape.msh"));
        const int SPACE_J=pt.get<int>("refine",3);
        const int TIME_i = pt.get<int>("time_refine",9);;
        const int noOfTimeSteps = pow(2,TIME_i);
        const double mu_crit_0 = sqrt(2)/3;//because ofg backward euler
        grid->globalRefine(SPACE_J+1);
        typedef GridType::LeafGridView GridView;
        typedef GridType::LevelGridView GV;

        Dune::Functions::LagrangeBasis<GV, 1> fineBasis(grid->levelGridView(SPACE_J));
        typedef  Dune::Functions::LagrangeBasis<GV, 1> Basis;
        auto sourceTerm = [](const Dune::FieldVector<double, dim> &x) {
            if (std::abs(x[0] - 0.5) < 0.25 && std::abs(x[1] - 0.5) < 0.25)
                return 1.0;
            return 0.0;
        };
        VectorType rhs,rhs1,rhs2;
        MatrixType matrix,small; //, massMatrix;
        assembleHeatEquationProblemForMethodOfLinesWithoutInvert(fineBasis, matrix,
                                                                 small,
                                                                 rhs, sourceTerm, noOfTimeSteps, 1.0/((double)noOfTimeSteps));
        rhs1.resize(small.N());
        rhs2.resize(small.N());
        for(int i=0;i<rhs1.size();i++){
            rhs1[i]=rhs[i];
        }
        if(pt.hasKey("i")){
            loadFromFile(rhs1,pt.get<std::string>("i","input.vector"));
        }
        small.mv(rhs1,rhs2);

        for(int i=0;i<rhs2.size();i++){
            rhs[i]=rhs2[i];
        }
        //MatrixType restriction;
        //getTimeRestriction(basis,2000,0.001,0.001,restriction);
        VectorType u_0,u_02;
        u_0.resize(rhs.size());
        u_02.resize(rhs.size());
        /*typedef RichardsonIterationSmoother<MatrixType,VectorType> RSmoother;
        RSmoother smoother(250);*/
        typedef Dune::SeqJac<MatrixType,VectorType,VectorType > SeqJac;
        typedef SmootherProxy<SeqJac,MatrixType> RSmoother;
        int smootherCalls =pt.get<int>("smootherCalls",500);
        RSmoother smoother([smootherCalls](const MatrixType &m,size_t j,size_t i){
            return SeqJac(m,smootherCalls,.5);
        });
        typedef SpaceTimeMultiGridMethodDriver<Dune::Functions::LagrangeBasis<GV, 1>,MatrixType,VectorType,RSmoother,RSmoother,double> SpacetimeDriver;
        SpacetimeDriver::BasisFactory basisFactory= [&](int i){
            return grid->levelGridView(i);
        };
        SpacetimeDriver space_time([&grid,&sourceTerm](int time_i,int space_j){
            MatrixType matrix,small; //, massMatrix;
            Basis b(grid->levelGridView(space_j));
            VectorType rhs;
        assembleHeatEquationProblemForMethodOfLinesWithoutInvert(b, matrix,
                                                                 small,
                                                                 rhs, sourceTerm, pow(2,time_i), pow(0.5,(double) time_i));

        return matrix;
        },basisFactory,SPACE_J,smoother,smoother,mu_crit_0,[&](size_t j){
            return pow(.5,(2.0+j));//WARN(henrik): should be rewritten, only works on const
        },[&](size_t j){
            return pow(0.5,j);
        });
        space_time.solve(u_0,rhs,TIME_i,SPACE_J,1);
        auto start = std::chrono::high_resolution_clock::now();
        space_time.solve(u_0,rhs,TIME_i,SPACE_J,1);
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> dur = end-start;
        std::cout<<dur.count()<<std::endl;
        writeToFile(u_0,pt.get<std::string>("o","output.test"));

        VectorType resultStep2,resultStep;
        resultStep2.resize(fineBasis.size());
        resultStep.resize(fineBasis.size());
        for (size_t i = 0; i < noOfTimeSteps; i++)
        {
            for (size_t k = 0; k < fineBasis.size(); k++)
            {
                resultStep2[k] = u_0[i * fineBasis.size() + k];
                //resultStep[k] = u_02[i * fineBasis.size() + k];

            }
            Dune::VTKWriter<GV> vtkWriter(fineBasis.gridView());
            vtkWriter.addVertexData(resultStep2, "solution");
            //vtkWriter.addVertexData(resultStep, "exact");
            std::stringstream str;
            str << i;
            vtkWriter.write("jac50_10_multigrid" + str.str());
        }
    }
    //catch (const std::exception &e)
    {
      //  std::cerr << e.what() << '\n';
    }
}