#ifndef PROLONGATION_HH
#define PROLONGATION_HH
#include <dune/istl/matrixindexset.hh>
//Halfs a time step
// noOfTimesteps therefore has to be divisable by 2
template <class Basis>
void getOccupationPatternforTimeRestriction(const Basis &basis, Dune::MatrixIndexSet &nb, size_t noOfTimesteps)
{
    nb.resize(basis.size() * noOfTimesteps / 2, basis.size() * noOfTimesteps);
    auto gridView = basis.gridView();
    auto localView = basis.localView();
    for (const auto &element : Dune::elements(gridView))
    {
        localView.bind(element);
        for (size_t i = 0; i < localView.size(); i++)
        {
            auto row = localView.index(i);
            //for (size_t j = 0; j < localView.size(); j++)
            {
                auto col = row;//localView.index(j);
                for (size_t k = 0; k < noOfTimesteps / 2; k++)
                {
                    nb.add(k * basis.size() + row, 2 * k * basis.size() + col);
                    nb.add(k * basis.size() + row, (2 * k + 1) * basis.size() + col);
                }
            }
        }
    }
}

//Halfs a time step
// noOfTimesteps therefore has to be divisable by 2
// A is the Matrix in the finer timestep
//R is the prolongation matrix
//M1_tauL is defined in (timestepSize)*stiffnessTime; (stiffnessTime=1, time StepSize=deltaT))
template <class M, class Basis>
void getTimeRestriction(Basis basis, size_t noOfTimesteps, const double deltaT, const double M1_TauL, M &R)
{
    auto gridView = basis.gridView();
    auto localView = basis.localView();
    const double Mtilde1_tauL = deltaT; //constant because of p_t= 0(backward euler) it is constant and there functions, but is it correct, that for different time scales they are equal
    const double Mtilde2_tauL = deltaT;
    const double R1 = Mtilde1_tauL / M1_TauL;
    const double R2 = Mtilde2_tauL / M1_TauL;
    Dune::MatrixIndexSet mIS;
    getOccupationPatternforTimeRestriction(basis, mIS, noOfTimesteps);
    mIS.exportIdx(R);
    R = 0;
    for (const auto &element : Dune::elements(gridView))
    {
        localView.bind(element);
        for (size_t i = 0; i < localView.size(); i++)
        {
            auto row = localView.index(i);
            //for (size_t j = 0; j < localView.size(); j++)
            {
                auto col = row;//localView.index(j);
                for (size_t k = 0; k < noOfTimesteps / 2; k++)
                {
                    R[k * basis.size() + row][2 * k * basis.size() + col] = R1;//QUESTION(henrik): Wouldn't it suffice to use indices one (eliminate one for loop?)
                    R[k * basis.size() + row][((2 * k) + 1) * basis.size() + col] = R2;//QUESTION(henrik): Wouldn't it suffice to use indices one (eliminate one for loop?)
                }
            }
        }
    }
}

//Halfs a time step AND space at the same time
// noOfTimesteps therefore has to be divisable by 2
template <class Basis>
void getOccupationPatternforSpaceTimeRestriction(const Basis &coarser, const Basis &finer, Dune::MatrixIndexSet &nb, size_t noOfTimesteps)
{
    nb.resize(coarser.size() * noOfTimesteps / 2, finer.size() * noOfTimesteps);
    auto finerGridView = finer.gridView();
    auto coarserGridView = coarser.gridView();
    auto finerlocalView = finer.localView();
    auto coarserlocalView = coarser.localView();
    for (const auto &element : Dune::elements(finerGridView))
    {
        finerlocalView.bind(element);
        auto father = element.father();
        coarserlocalView.bind(father);
     //   std::cout << finerlocalView.size() << ":" << coarserlocalView.size() << std::endl;
        for (size_t i = 0; i < coarserlocalView.size(); i++)
        {
            auto row = coarserlocalView.index(i);
            for (size_t j = 0; j < finerlocalView.size(); j++)
            {
                auto col = finerlocalView.index(j);
                for (size_t k = 0; k < noOfTimesteps / 2; k++)
                {
                    nb.add(k * coarser.size() + row, 2 * k * finer.size() + col);//QUESTION(henrik): Wouldn't it suffice to use indices one (eliminate one for loop?)
                    nb.add(k * coarser.size() + row,  (2 *k+1) * finer.size() + col);
                }
            }
        }
    }
}
 class CoordinateEvaluation
  {
  public:
  struct Traits
    {
      typedef  double RangeType;
    };
    // store the coordinate to evaluate
    CoordinateEvaluation (int i_) : i(i_) {}

    // eval coordinate i
    template<typename DT, typename RT>
    inline void evaluate (const DT& x, RT& y) const
    {
      y = x[i];
      return;
    }

  private:
    int i;
  };
//Halfs a time step
// noOfTimesteps therefore has to be divisable by 2
// A is the Matrix in the finer timestep
//R is the prolongation matrix
//M1_tauL is defined in (timestepSize)*stiffnessTime; (stiffnessTime=1, time StepSize=deltaT))
template <class M, class Basis>
void getSpaceTimeRestriction(Basis finer, Basis coarser, size_t noOfTimesteps, const double deltaT, const double M1_TauL, M &R)
{
    auto finerGridView = finer.gridView();
    const int dim = Basis::GridView::dimension;
    auto coarserGridView = coarser.gridView();
    auto finerlocalView = finer.localView();
    auto coarserlocalView = coarser.localView();
    const double Mtilde1_tauL = deltaT; //constant because of p_t= 0(backward euler) it is constant and there functions, but is it correct, that for different time scales they are equal
    const double Mtilde2_tauL = deltaT;
    const double R1 = Mtilde1_tauL / M1_TauL;
    const double R2 = Mtilde2_tauL / M1_TauL;
    Dune::MatrixIndexSet mIS;
    getOccupationPatternforSpaceTimeRestriction(coarser, finer, mIS, noOfTimesteps);
    mIS.exportIdx(R);
    R = 0;
    for (const auto &element : Dune::elements(finerGridView))
    {
        finerlocalView.bind(element);
        auto father = element.father();
        coarserlocalView.bind(father);
        // determine local coordinates of vertices in fine element
        std::vector<Dune::FieldVector<double,dim> > local_position(finerlocalView.size());
        for (int k=0; k<dim; k++)
          {
            CoordinateEvaluation f(k);
            std::vector<double> c(finerlocalView.size());
            finerlocalView.tree().finiteElement().localInterpolation().interpolate(f,c);
            for (size_t i=0; i<finerlocalView.size(); i++)
              local_position[i][k] = c[i];
          }
        // determine local coordinates of vertices in father element
        std::vector<Dune::FieldVector<double,dim> > local_position_in_father(finerlocalView.size());
        for (size_t i=0; i<finerlocalView.size(); i++)
          local_position_in_father[i] = element.geometryInFather().global(local_position[i]);
        // interpolation weights are values of coarse grid basis functions at fine grid points
        for (size_t i = 0; i < finerlocalView.size(); i++)
        {
            typedef Dune::FieldVector<double, 1> RT;
            std::vector<RT> phi(coarserlocalView.size());
            coarserlocalView.tree().finiteElement().localBasis().evaluateFunction(local_position_in_father[i],phi);
            auto col = finerlocalView.index(i);
            for (size_t j = 0; j < coarserlocalView.size(); j++)
            {
                auto row = coarserlocalView.index(j);
                //if (phi[j]>1E-6)
                for (size_t k = 0; k < noOfTimesteps / 2; k++)
                {

                    R[k * coarser.size() + row][ 2 *k * finer.size() + col]=R1*phi[j][0];
                    R[k * coarser.size() + row][ ((2 * k)+1) * finer.size() + col]=R2*phi[j][0];
                }
            }
        }
    }
}
#endif