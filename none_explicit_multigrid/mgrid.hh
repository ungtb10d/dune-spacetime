#include <dune/istl/operators.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include "prolongation.hh"
#include <dune/istl/eigenvalue/poweriteration.hh>
//TODO: individual solver!, adaptive richardson iteration
//this is based on parsolve MultigridOnOverlappinggrids
template <class GridHierarchy, class MatrixHierarchy, class VectorType, class PreSmoother, class PostSmoother, typename ctype>
class MultiGridMethod
{
    typedef typename MatrixHierarchy::Matrix M;
    typedef Dune::PDELab::Backend::Native<VectorType> NVT;
    typedef Dune::PDELab::Backend::Native<M> NM;
    ProlongationOperatorHierarchy<GridHierarchy> poh;
    //    size_t nu_1, nu_2;
    const MatrixHierarchy &A;
    const GridHierarchy &gh;
    PreSmoother &preSmoother; //not const, because they might be adaptive or so
    PostSmoother &postSmoother;
    int J;

public:
    MultiGridMethod(const GridHierarchy &_gh, const MatrixHierarchy &_A, PreSmoother &_pre_smoother, PostSmoother &_post_smoother) : gh(_gh),
                                                                                                                                     preSmoother(_pre_smoother), postSmoother(_post_smoother), A(_A), poh(_gh), J(gh.maxLevel())
    {
    }
    void apply(VectorType &v, const VectorType &d)
    {
        MGM(J, v, d);
    }

private:
    void MGM(size_t j, VectorType &xk_j, VectorType b_j) //TODO(henrik): omega übergeben
    {
        using Dune::PDELab::Backend::native;
        auto A_j = A[j];
        std::cout << "A_" << j << ":" << native(A_j).M() << "x" << native(A_j).N() << std::endl;
        //std::ofstream datei("debug_File.txt");

        if (j == 0)
        {
            std::cout << "SOLVING" << std::endl;

            // Solve linear system taken from sheet 02 FEM 201920

           /* typedef Dune::MatrixAdapter<NM, NVT, NVT> LinearOperator;
            LinearOperator op(native(A_j));
            typedef Dune::SeqILU<NM, NVT, NVT> Preconditioner;
            Preconditioner prec(native(A_j), 1.8);

            const ctype reduction = 1e-9;
            const int max_iterations = 10000;
            const int verbose = 1;

            typedef Dune::CGSolver<NVT> Solver;
            Solver solver(op, prec, reduction, max_iterations, verbose);
            */
           Dune::UMFPack<NM> solver(native(A_j),0);
            Dune::InverseOperatorResult result;
            solver.apply(native(xk_j), native(b_j), result);
            return;
        }

        //auto xk_j1 = xk_j;
        preSmoother.apply(xk_j, b_j, A_j, j);
        VectorType d_j(gh.getGFS(j));
        d_j = b_j;
        native(A_j).mmv(native(xk_j), native(d_j)); //defect calculation
        //CGM R;//coarsening matrix
        VectorType d_j_1(gh.getGFS(j - 1), 0.0);
        poh[j].mtv(native(d_j), native(d_j_1)); // restriction
        //TODO(henrik): currently V-Cycle only here
        VectorType x_j_1(gh.getGFS(j - 1), 0.0);
        MGM(j - 1, x_j_1, d_j_1);
        //std::cout << "E" << std::endl;
        VectorType y_j(gh.getGFS(j), 0.0);
        poh[j].mv(native(x_j_1), native(y_j)); //Prolongation, QUESTION(Multiply by factor in order to reset restriction)
        xk_j += y_j;                           //coarse grid correction;
                                               //Dune::printvector(datei, xk_j1, "xk_j1", "", 1, 24, 4);
                                               //TODO(henrik): avoid copys
        postSmoother.apply(xk_j, b_j, A_j, j);
    }
};