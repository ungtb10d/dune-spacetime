import matplotlib.pyplot as plt
import os
import numpy as np
def smoothers():
    os.system("./none_explicit_sp/none_explicit_sp -timestepcount 512 -o reference.vector -i globe512.vector")
    differences = []
    for  i in range(12):
        os.system("./none_explicit_sp_multigrid/none_explicit_sp_mg -smootherCalls "+str(50*(i+1))+" -o "+str(50*(i+1))+".vectorx -i globe512.vector")
        print("calc")
        stream = os.popen("./difference/difference -a reference.vector -b "+str(50*(i+1))+".vectorx" )
        res = stream.read()
        print(res)
        differences += [float(res)]
        print(differences)
    plt.plot([(i+1)*50 for i in range(12)],differences,'ro-')
    plt.ylabel("Difference")
    plt.xlabel("Pre/Post-Smoothing steps")
    plt.show()
def mu(time,space,T): #TODO(henrik): Add T BUG(henrik): only twod!
    #\tau_L*(h_L)²
    return (2.0**(2*space- time)*T)
# for time level 8, and line.10, const nux and nut, solve two grid
def discretization_conv(Ts):
    mus = []
    conv = []
    for T in Ts:
        print("T:",T,",µ:",mu(8,10,T))
        #direct solve
        s1= os.popen("./build-cmake/none_explicit_forward/none_explicit_forward -T "+str(T)+" -o build-cmake/exact.vectorx -timestepcount 256 -refine 10 -i build-cmake/line.10")
        print("two grid")
        s2= os.popen("./build-cmake/none_explicit_sp_multigrid_multigrid/none_explicit_sp_mg_mg -T " +str(T)+"  -o build-cmake/10.time256.upto250.line.vectorx8 -time_refine 8 -refine 10 -i build-cmake/line.10 -k 50 -nut 2 -nux 5  -solveTimeAtLevel 7")
        s1.read()
        s2.read()
        prevE= 1.0
        maxE = 0.0
        for k in range(50):
            stream = os.popen("./build-cmake/residual/residual -a build-cmake/10.time256.upto250.line.vectorx8.v"+str(k+1)+" -b build-cmake/exact.vectorx" )
            err = float(stream.read())
            if err <1e-8:
                print("k",k)
                break
            if ((k!=0) and maxE<err/prevE) or (k==1):
                print(err/prevE)
                maxE = err/prevE
            prevE = err
        mus += [mu(8,10,T)]
        conv += [maxE]
        print(mus)
        print(conv)
    return mus,conv
#3D, fixed spacial discretization, fixed T
def runtime_threed(time_is):
    tbb = []
    no_tbb= []
    direct=[]
    multigrid =[]
    T=1
    # render problem
    os.system("./build-cmake/renderfunction/render -o ./build-cmake/problem.3d -refine 3 -threed 1")
    for i in time_is:
        print("Timesteps:",2**i)
        s1= os.popen("./build-cmake/none_explicit_forward/none_explicit_forward -T "+str(T)+" -o build-cmake/exact.vectorx+"+str(i)+" -timestepcount "+str(2**i)+" -refine 3 -i build-cmake/problem.3d -threed 1")
        direct += [float(s1.read())]
        s2 = os.popen("./build-cmake/none_explicit_sp_multigrid_multigrid/none_explicit_sp_mg_mg -T 1 -o const_mg.vectorxd"+str(i)+" -time_refine "+str(i)+" -refine 3 -i build-cmake/problem.3d -k 1 -nut 2 -nux 2 -solveTimeAtLevel 0 -threed 1")
        tbb +=[float(s2.read())]
        s3 = os.popen("./build-cmake/none_explicit_sp_multigrid_multigrid/none_explicit_sp_mg_mg_notbb -T 1 -o no_tbbconst_mg.vectorxd"+str(i)+" -time_refine "+str(i)+" -refine 3 -i build-cmake/problem.3d -k 1 -nut 2 -nux 2 -solveTimeAtLevel 0 -threed 1")
        no_tbb +=[float(s3.read())]
        s4 = os.popen("./build-cmake/none_explicit_multigriddirect/none_explicit_mg_direct -T 1 -o no_tbbconst_mg.vectorxd"+str(i)+" -time_refine "+str(i)+" -refine 3 -i build-cmake/problem.3d -k 1 -nut 2 -nux 2 -solveTimeAtLevel 0 -threed 1")
        multigrid +=[float(s4.read())]
        print(direct,tbb,no_tbb, multigrid)
    return [2**i for i in time_is],direct,no_tbb,tbb, multigrid

def runtime_oned(time_is):
    tbb = []
    no_tbb= []
    direct=[]
    multigrid =[]
    T=1
    # render problem
    os.system("./build-cmake/renderfunction/render -o ./build-cmake/problem.1d -refine 8 -oned 1 -cube 1")
    for i in time_is:
        print("Timesteps:",2**i)
        print("./build-cmake/none_explicit_forward/none_explicit_forward -T "+str(T)+" -o build-cmake/exact.vectorx+"+str(i)+" -timestepcount "+str(2**i)+" -refine 8 -i build-cmake/problem.1d -oned 1 -cube 1")
        s1= os.popen("./build-cmake/none_explicit_forward/none_explicit_forward -T "+str(T)+" -o build-cmake/exact.vectorx+"+str(i)+" -timestepcount "+str(2**i)+" -refine 8 -i build-cmake/problem.1d -oned 1 -cube 1")
        direct += [float(s1.read())]
        print("./build-cmake/none_explicit_sp_multigrid_multigrid/none_explicit_sp_mg_mg -T 1 -o const_mg.vectorxd"+str(i)+" -time_refine "+str(i)+" -refine 8 -i build-cmake/problem.1d -k 1 -nut 2 -nux 2 -solveTimeAtLevel 0 -oned 1 -cube 1")
        s2 = os.popen("./build-cmake/none_explicit_sp_multigrid_multigrid/none_explicit_sp_mg_mg -T 1 -o const_mg.vectorxd"+str(i)+" -time_refine "+str(i)+" -refine 8 -i build-cmake/problem.1d -k 1 -nut 2 -nux 2 -solveTimeAtLevel 0 -oned 1 -cube 1")
        tbb +=[float(s2.read())]
        s3 = os.popen("./build-cmake/none_explicit_sp_multigrid_multigrid/none_explicit_sp_mg_mg_notbb -T 1 -o no_tbbconst_mg.vectorxd"+str(i)+" -time_refine "+str(i)+" -refine 8 -i build-cmake/problem.1d -k 1 -nut 2 -nux 2 -solveTimeAtLevel 0 -oned 1 -cube 1")
        no_tbb +=[float(s3.read())]
        s4 = os.popen("./build-cmake/none_explicit_multigriddirect/none_explicit_mg_direct -T 1 -o no_tbbconst_mg.vectorxd"+str(i)+" -time_refine "+str(i)+" -refine 8 -i build-cmake/problem.1d -k 1 -nut 2 -nux 2 -solveTimeAtLevel 0 -oned 1 -cube 1")
        multigrid +=[float(s4.read())]
        print(direct,tbb,no_tbb, multigrid)
    return [2**i for i in time_is],direct,no_tbb,tbb, multigrid
def errors(time_res,space_res,smootherSteps =1000):
    def calcDOFs(time,space):
        return ((2*2**space+1)**2)*(2**time)
    print(calcDOFs(9,3))
    differences = []
    durations = []
    dofss = []
    for j in space_res:
        differences += [[]]
        durations += [[]]
        dofss += [[]]
        for i in time_res:
            if calcDOFs(i,j)<1000000:
                print("calc ",calcDOFs(i,j))
                direct= os.popen("OMP_NUM_THREADS=1 ./none_explicit_sp/none_explicit_sp -timestepcount "+str(2**i)+" -o direct_"+str(i)+"_"+str(j)+".vectorx -refine "+str(j))
                mg= os.popen("./none_explicit_sp_multigrid/none_explicit_sp_mg -smootherCalls "+str(smootherSteps)+" -o "+str(i)+"_"+str(j)+".vectorx -refine "+str(j)+" -time_refine "+str(i))
                mg_res = mg.read()
                direct_res = direct.read()
                stream = os.popen("./difference/difference -a direct_"+str(i)+"_"+str(j)+".vectorx -b "+str(i)+"_"+str(j)+".vectorx" )
                res = stream.read()
                print(mg_res)
                print(direct_res)
                print(res)
                durations[len(durations)-1] += [[float(direct_res),float(mg_res)]]
                differences[len(differences)-1] += [float(res)]
                dofss[len(dofss)-1] += [calcDOFs(i,j)]
                print(differences,durations,dofss)
        print(differences,durations,dofss)
    return (differences,durations,dofss)
def discretization(space_res, time_res):
    def calcDOFs(time,space):
        return ((2*2**space+1)**2)*(2**time)
    print(calcDOFs(9,3))
    rels = []
    dofs = []
    mus = []
    for j in space_res:
        rels += [[]]
        dofs += [[]]
        mus += [[]]
        os.system("./renderfunction/render -o globe."+str(j)+" -refine "+str(j))
        # render function to grid
        for i in time_res:
            if calcDOFs(i,j)<10000000:
                print("mmu: ",mu(i,j))
                print("CalcDOFS",calcDOFs(i,j))
                forward= os.popen("OMP_NUM_THREADS=1 ./none_explicit_forward/none_explicit_forward -T 1 -timestepcount "+str(2**i)+" -o direct_"+str(i)+"_"+str(j)+".vectorx -refine "+str(j)+" -i globe."+str(j))
                mg= os.popen("./none_explicit_sp_multigrid/none_explicit_sp_mg -T 1 -k 3 -o "+str(i)+"_"+str(j)+".vectorx -refine "+str(j)+" -time_refine "+str(i)+" -i globe."+str(j))
                mg_res = mg.read()
                forward_res = forward.read()
                stream = os.popen("./difference/difference -a direct_"+str(i)+"_"+str(j)+".vectorx -b "+str(i)+"_"+str(j)+".vectorx" )
                stream1 = os.popen("./difference/difference -a direct_"+str(i)+"_"+str(j)+".vectorx -b "+str(i)+"_"+str(j)+".vectorx -full 1" )
                res_1= stream1.read()
                full = float(res_1)
                print(full)
                res = stream.read()
                diff = float(res)
                rel = diff/full
                print(rel)
                print(forward_res)
                print(res)
                rels[len(rels)-1] += [rel]
                mus[len(mus)-1] += [mu(i,j)]
                dofs[len(dofs)-1] += [calcDOFs(i,j)]
                print(rels)
                print(mus)
                print(dofs)
    return rels,mus,dofs
#smoothers()
#dif, dur, dofs = errors([7,8,9,10,11],[2,3,4],5)
#print(discretization([3,4,5],[1,2,3,4,7,8,9,10]))
ks = np.arange(1,60)
MaxNu =2
naus=  [1,2,3,5]
def absolute_error(gamma=[1,2]):
    differences = []
    streamF = os.popen("./build-cmake/difference/difference -a build-cmake/fw.vectorx -b build-cmake/fw.vectorx -full 1" )
    absolute= float(streamF.read())
    maxVers = []
    for nau in naus:
        differencesNau = []
        maxVer = -1
        skip = False
        for k in ks:
            if k%10==0:
                print(k)
            stream = os.popen("./build-cmake/difference/difference -a build-cmake/mg_mg.4.time256.upto250.vectorx.nu"+str(nau)+".v"+str(k)+" -b build-cmake/fw.vectorx" )
            differencesNau += [float(stream.read())]
            if len(differencesNau)>1:
                # calc relative error reduction
                old = differencesNau[len(differencesNau)-2]
                new = differencesNau[len(differencesNau)-1]
                rer = (old - new)
                print(rer)
                ver = new/old
                if (maxVer== -1 or maxVer > ver) and not skip:
                    maxVer = ver
                if abs(rer)<10e-16:
                    print("RELATIVE error",abs(rer) )
                    skip = True
        differences += [differencesNau]
        maxVers +=[maxVer]
    print(maxVers)
    return differences
def render_absoluteerror():
    print(mu(8,10,0.0001))
    diff = absolute_error()
    for nau in range(len(diff)):
        plt.loglog(ks,diff[nau],label=str(naus[nau]))
    plt.legend()
    plt.xlabel("k")
    plt.ylabel("absolute error")
    plt.show()
def render_discretization_conv():
    m,conv = discretization_conv([0.000001,0.00001,0.0001,0.001,0.01,0.1,1,10,100,1000])
    plt.loglog(m,conv)
    plt.legend()
    plt.xlabel("mu")
    plt.ylabel("conv")
    plt.show()
def render_runtime():
    time_steps,direct,notbb,tbb, multigrid =runtime_oned([1,2,3,4,5,6,7,8,9,10,11])#[2**i for i in [1,2,3,4,5,6,7,8,9]], [0.33395, 0.784057, 1.55259, 3.0592, 6.10041, 13.367, 25.1009, 50.8516, 101.414], [0.495937, 0.73707, 1.09655, 1.82182, 3.3596, 6.48055, 12.9628, 34.8841, 78.243], [0.531112, 1.04316, 2.1388, 4.30983, 8.57153, 17.4664, 35.8559, 99.2679, 226.634]
    plt.loglog(time_steps,direct,label="direct")
    plt.loglog(time_steps,notbb, label="Single Threaded")
    plt.loglog(time_steps,tbb, label="TBB")
    plt.loglog(time_steps,multigrid, label="Multigrid")
    plt.legend()
    plt.xlabel("time steps")
    plt.ylabel("time")
    plt.show()
render_runtime()