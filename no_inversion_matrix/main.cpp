#include <config.h>
#include <dune/common/parallel/mpihelper.hh>
#define HAVE_UG 1
//#define BORDER_WARNING
#include <cmath>
const int dim = 2;
const int refinement_level = 6;
#include "config.h"
#include <dune/grid/yaspgrid.hh>
#include <dune/istl/matrix.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/io.hh>
#include <fstream>
#include "helpers.hh"
#include "output.hh"
#include <array>
#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/istl/bdmatrix.hh>
#include <dune/istl/matrixmatrix.hh>
#include <dune/istl/eigenvalue/poweriteration.hh>
#include "multigrid.hh"
typedef double ctype;
typedef Dune::BCRSMatrix<Dune::FieldMatrix<ctype, 1, 1>> Matrix;
typedef Dune::BDMatrix<Dune::FieldMatrix<ctype, 1, 1>> BDMatrix;
//number of coefficient describing the local basis functions
const int coeff_size = 1;
std::map<size_t, double> map_eigen;
template <typename M, typename ct>
ct calcEigenvalues(M matrix, size_t j)
{
    if (map_eigen.count(j) > 0)
    { //return early if we already have the stiffness Matrix calculated
        return map_eigen.at(j);
    }
    auto result = Dune::PowerIteration_Algorithms<Matrix, Dune::DynamicVector<ct>>(matrix, 20000, 2);
    ct dominant;
    Dune::DynamicVector<double> ev(matrix.N(), 1);
    //1e-1 exppecting huge eigenvalues
    result.applyPowerIteration(1e-9, ev, dominant);
    std::cout << dominant << std::endl;
   // std::cin>> dominant;
    map_eigen[j] = dominant;
    return dominant;
}

//testing with 1*1 field divided into 2^grid_depth*2^grid_depth*2 triangles
template <typename ct>
void MGM_driver(size_t timesteps, size_t grid_depth, const size_t nu_1, const size_t nu_2, std::vector<Dune::DynamicVector<ct>> *results_over_time, std::function<void(Dune::DynamicVector<ct> &)> callback)
{

    //expect M*N, residual error
    //for now only identy for A in the term
    //using P_1 elements

    const size_t dim = (pow(2, grid_depth) + 1) * (pow(2, grid_depth) + 1) * coeff_size;
    std::cout << "DOF" << dim << std::endl;
    typedef Dune::DynamicVector<ct> IV;
    typedef Matrix SM;
    typedef Dune::DynamicMatrix<ct> CGM;
    IV u_0(dim, 0);
    IV initialV(dim, 0);
    const size_t width = pow(2, grid_depth) + 1;
    const size_t corners = (pow(2, grid_depth) - 1);
    for (size_t k = 10; k < corners - 10; k++)
    //for (size_t k = 0; k < corners; k++)
    { //left to right
        for (size_t l = 10; l < corners - 10; l++)
        //for (size_t l = 0; l < corners; l++)
        {
            //bottom to top
            //set 6 values
            double val = 1.0; //*rand() / (double)RAND_MAX; //l*corners+k;
            //std::cout << val << std::endl;
            accessV2D<IV, double>(initialV, k, l, false, 2, width) = val;         //lower
            accessV2D<IV, double>(initialV, k, l, true, 2, width) = val;          //2
            accessV2D<IV, double>(initialV, k + 1, l, true, 0, width) = val;      //3
            accessV2D<IV, double>(initialV, k, l + 1, false, 1, width) = val;     //4
            accessV2D<IV, double>(initialV, k + 1, l + 1, false, 0, width) = val; //5
            accessV2D<IV, double>(initialV, k + 1, l + 1, true, 1, width) = val;  //6
        }
    }
    //start with initial values
    results_over_time->push_back(initialV);
    callback(initialV);

    std::map<size_t, SM> map_Ah;
    std::map<size_t, SM> map_M;
    /*
  * Problem is given by Bastian chapter 11
  * \partial_t u - \nabla\cdot (A\nabla u)=f in \Omega\times \Sigma
  * u=0 on\partial \Omega\times\Sigma
  * u(x,t_0)=u_0 x\in \Omega
  * Derive weak formulation and insert basis function, for now just using the reference grid in total
  * using MoL and b(t)=0,
  * adjusting for
  */
    std::function<SM(size_t)> A_h = [&](size_t j) {
        if (map_Ah.count(j) > 0)
        { //return early if we already have the stiffness Matrix calculated
            return map_Ah.at(j);
        }
        // size_t dim = finest_grid_width  / std::pow(4, grid_depth - j); //4 because we have to go 2 by each side
        size_t boxes_in_row = pow(2, j);
        std::cout << "boxes:" << boxes_in_row << std::endl;
        const size_t points_in_row = (boxes_in_row + 1);
        size_t points = (boxes_in_row + 1) * (boxes_in_row + 1);
        SM A(points, points, 14 * points, 2.0, Matrix::BuildMode::implicit);
        const ctype det = 1.0 / (pow((double)boxes_in_row, 2));
        const ctype m_phi_jphi_j = (1 / 12.0) * det;
        std::cout << "m_phi_jphi_j:" << m_phi_jphi_j << std::endl;
        const ctype m_phi_jphi_j_outer = (1 / 24.0) * det;
        std::cout << "m_phi_jphi_j_outer:" << m_phi_jphi_j_outer << std::endl;
        const ctype abl_ = -0.5 * det; //TODO(henrik): Vorzeichen?
        const ctype fac_middle = 1.0;
        const ctype fac_a_c = 0.5;
        //new variables
        //N = nabla
        const ctype Nphi_A_Nphi_B = -0.5 * det;
        const ctype Nphi_A_Nphi_C = 0 * det;
        const ctype Nphi_B_Nphi_C = -0.5 * det;
        //total
        const ctype Nphi_i_Nphi_i = 4.0 * det;
        //we have
        // a) edge condition zero
        //and 3 basis function
        //how to  numerate the basis functions?--> see graphic
        //bottom to top
        //iterate over border
        for (size_t k = 0; k < points_in_row; k++)
        {
            for (size_t l = 0; l < points_in_row; l++)
            {
                A.entry(k * points_in_row + l, k * points_in_row + l) = 0.0; //unsingular
            }
        }
        int no_n(0),no_no(0), no_s(0),no_sw(0),no_w(0),no_o(0);
        for (size_t k = 1; k < points_in_row - 1; k++)
        { //BUG(henrik): Optimize here!
            //left to right
            for (size_t l = 1; l < points_in_row - 1; l++)
            {
                A.entry(indexInArr(k, l, false, 0, points_in_row), indexInArr(k, l, false, 0, points_in_row)) = Nphi_i_Nphi_i;
                //six neighbors
                // N(orth) : A/B+B/C
                if (l < points_in_row - 2)
                {
                     A.entry(indexInArr(k, l, false, 0, points_in_row), indexInArr(k, l + 1, false, 0, points_in_row)) = Nphi_A_Nphi_B + Nphi_B_Nphi_C;
                    A.entry(indexInArr(k, l + 1, false, 0, points_in_row), indexInArr(k, l, false, 0, points_in_row)) = Nphi_A_Nphi_B + Nphi_B_Nphi_C;
                }else {
                    std::cout<<" NO N:"<<k<<" "<<l<<std::endl;
                    no_n++;
                }
                if ((l < (points_in_row - 2)) && (k < (points_in_row - 2)))
                {
                    A.entry(indexInArr(k, l, false, 0, points_in_row), indexInArr(k + 1, l + 1, false, 0, points_in_row)) = Nphi_A_Nphi_C + Nphi_B_Nphi_C;
                    A.entry(indexInArr(k + 1, l + 1, false, 0, points_in_row), indexInArr(k, l, false, 0, points_in_row)) = Nphi_A_Nphi_C + Nphi_B_Nphi_C;
                }
                else {
                    std::cout<<" NO NO:"<<k<<" "<<l<<std::endl;
                    no_no++;
                }
                if (k < (points_in_row - 2))
                {
                    A.entry(indexInArr(k, l, false, 0, points_in_row), indexInArr(k + 1, l, false, 0, points_in_row)) = Nphi_A_Nphi_B + Nphi_A_Nphi_C;
                    A.entry(indexInArr(k+1, l , false, 0, points_in_row), indexInArr(k, l, false, 0, points_in_row)) = Nphi_A_Nphi_B + Nphi_A_Nphi_C;
                }else {
                    std::cout<<" NO O:"<<k<<" "<<l<<std::endl;
                    no_o++;
                }
                if (l > 1)
                {
                    // S(outh) : A/B+A/C
                    A.entry(indexInArr(k, l, false, 0, points_in_row), indexInArr(k, l - 1, false, 0, points_in_row)) = Nphi_A_Nphi_B + Nphi_A_Nphi_C;
                    A.entry(indexInArr(k, l - 1, false, 0, points_in_row), indexInArr(k, l, false, 0, points_in_row)) = Nphi_A_Nphi_B + Nphi_A_Nphi_C;
                }else {
                    std::cout<<" NO S:"<<k<<" "<<l<<std::endl;
                    no_s++;
                }
                if (l > 1 && k > 1)
                {
                    // SW(SouthWest) : A/C+B/C
                    A.entry(indexInArr(k, l, false, 0, points_in_row), indexInArr(k - 1, l - 1, false, 0, points_in_row)) = Nphi_A_Nphi_C + Nphi_B_Nphi_C;
                    A.entry(indexInArr(k - 1, l - 1, false, 0, points_in_row), indexInArr(k, l, false, 0, points_in_row)) = Nphi_A_Nphi_C + Nphi_B_Nphi_C;
                }else {
                    std::cout<<" NO SW:"<<k<<" "<<l<<std::endl;
                    no_sw++;
                }
                if (k > 1)
                {
                    // W(est) : A/B+A/C
                    A.entry(indexInArr(k, l, false, 0, points_in_row), indexInArr(k - 1,l , false, 0, points_in_row)) = Nphi_A_Nphi_C + Nphi_A_Nphi_B;
                    A.entry(indexInArr(k - 1, l , false, 0, points_in_row), indexInArr(k, l, false, 0, points_in_row)) = Nphi_A_Nphi_C + Nphi_A_Nphi_B;
                }
                else {
                    std::cout<<" NO W:"<<k<<" "<<l<<std::endl;
                    no_w++;
                }
            }
        }
        //TODO: Nullbedingung
        //print_to_file(A,"A_j");
        std::cout << "Built matrix. Going to invert..." <<points_in_row<< std::endl;
        // print_to_file(M, "M");
        //print_to_file(A, "A");
        //M.invert();
        std::cout << "NO N:"<< no_n<<std::endl;
        std::cout << "NO NO:"<< no_no<<std::endl;
        std::cout << "NO O:"<< no_o<<std::endl;
        std::cout << "NO S:"<< no_s<<std::endl;
        std::cout << "NO SW:"<< no_sw<<std::endl;
        std::cout << "NO W:"<< no_w<<std::endl;
        int count = 0;
        std::cout << count << std::endl;
        //print_to_file(M, "M_inv");
       // A *= 1.0/((double) timesteps);
        A.compress();
        map_Ah[j] = A; //save for later use
        return A;
    };
    std::function<SM(size_t)> M_h = [&](size_t j) {
        if (map_M.count(j) > 0)
        { //return early if we already have the stiffness Matrix calculated
            return map_M.at(j);
        }
        std::cout << "j:" << j << std::endl;
        // size_t dim = finest_grid_width  / std::pow(4, grid_depth - j); //4 because we have to go 2 by each side
        size_t boxes_in_row = pow(2, j);
        std::cout << "boxes:" << boxes_in_row << std::endl;
        const size_t points_in_row = (boxes_in_row + 1);
        size_t points = (boxes_in_row + 1) * (boxes_in_row + 1);
        SM M(points, points, 14* points, 2.0, Matrix::BuildMode::implicit);
        const ctype det = 1.0 / (pow((double)boxes_in_row, 2));
        const ctype m_phi_jphi_j = (1 / 12.0) * det;
        std::cout << "m_phi_jphi_j:" << m_phi_jphi_j << std::endl;
        const ctype m_phi_jphi_j_outer = (1 / 24.0) * det;
        std::cout << "m_phi_jphi_j_outer:" << m_phi_jphi_j_outer << std::endl;
        const ctype abl_ = -0.5 * det; //TODO(henrik): Vorzeichen?
        const ctype fac_middle = 1.0;
        const ctype fac_a_c = 0.5;
        //new variables
        const ctype phi_i_phi_i = 0.5 * det;
        const ctype phi_A_phi_B = (1 / 24.0) * det;
        const ctype phi_A_phi_C = (1 / 24.0) * det;
        const ctype phi_B_phi_C = (1 / 24.0) * det;
        //we have
        // a) edge condition zero
        //and 3 basis function
        //how to  numerate the basis functions?--> see graphic
        //bottom to top
        //iterate over border

        for (size_t k = 0; k < points_in_row; k++)
        {
            for (size_t l = 0; l < points_in_row; l++)
            {
                M.entry(k * points_in_row + l, k * points_in_row + l) = 1.0; //unsingular
            }
        }
        int no_n(0),no_no(0), no_s(0),no_sw(0),no_w(0),no_o(0);
        for (size_t k = 1; k < points_in_row - 1; k++)
        { //BUG(henrik): Optimize here!
            //left to right
            for (size_t l = 1; l < points_in_row - 1; l++)
            {
                //set center
                M.entry(indexInArr(k, l, false, 0, points_in_row),indexInArr(k, l, false, 0, points_in_row)) = phi_i_phi_i;
                //six neighbors
                // N(orth) : A/B+B/C
                if (l < points_in_row - 2)
                {
                    M.entry(indexInArr(k, l, false, 0, points_in_row),indexInArr(k, l + 1, false, 0, points_in_row)) = phi_A_phi_B + phi_B_phi_C;
                    M.entry(indexInArr(k, l + 1, false, 0, points_in_row),indexInArr(k, l, false, 0, points_in_row)) = phi_A_phi_B + phi_B_phi_C;
                   }else {
                    std::cout<<" NO N:"<<k<<" "<<l<<std::endl;
                    no_n++;
                }
                if ((l < (points_in_row - 2)) && (k < (points_in_row - 2)))
                {
                    // NO(orthOst) : A/C+B/C
                    M.entry(indexInArr(k, l, false, 0, points_in_row),indexInArr(k + 1, l + 1, false, 0, points_in_row)) = phi_A_phi_C + phi_B_phi_C;
                    M.entry(indexInArr(k + 1, l + 1, false, 0, points_in_row),indexInArr(k, l, false, 0, points_in_row)) = phi_A_phi_C + phi_B_phi_C;
               }
                else {
                    std::cout<<" NO NO:"<<k<<" "<<l<<std::endl;
                    no_no++;
                }
                if (k < (points_in_row - 2))
                {
                    // O(st) : A/B+A/C
                    M.entry(indexInArr(k, l, false, 0, points_in_row),indexInArr(k + 1, l, false, 0, points_in_row)) = phi_A_phi_B + phi_A_phi_C;
                    M.entry(indexInArr(k + 1, l, false, 0, points_in_row),indexInArr(k, l, false, 0, points_in_row)) = phi_A_phi_B + phi_A_phi_C;
                 }else {
                    std::cout<<" NO O:"<<k<<" "<<l<<std::endl;
                    no_o++;
                }
                if (l > 1)
                {
                    // S(outh) : A/B+A/C
                    M.entry(indexInArr(k, l, false, 0, points_in_row),indexInArr(k, l - 1, false, 0, points_in_row)) = phi_A_phi_B + phi_A_phi_C;
                    M.entry(indexInArr(k, l - 1, false, 0, points_in_row),indexInArr(k, l, false, 0, points_in_row)) = phi_A_phi_B + phi_A_phi_C;
                     }else {
                    std::cout<<" NO S:"<<k<<" "<<l<<std::endl;
                    no_s++;
                }
                if (l > 1 && k > 1)
                {
                    // SW(SouthWest) : A/C+B/C
                    M.entry(indexInArr(k, l, false, 0, points_in_row),indexInArr(k - 1, l - 1, false, 0, points_in_row)) = phi_A_phi_C + phi_B_phi_C;
                    M.entry(indexInArr(k - 1, l - 1, false, 0, points_in_row),indexInArr(k, l, false, 0, points_in_row)) = phi_A_phi_C + phi_B_phi_C;
                     }else {
                    std::cout<<" NO SW:"<<k<<" "<<l<<std::endl;
                    no_sw++;
                }
                if (k > 1)
                {
                    // W(est) : A/B+A/C
                    M.entry(indexInArr(k, l, false, 0, points_in_row),indexInArr(k - 1, l, false, 0, points_in_row)) = phi_A_phi_C + phi_A_phi_B;
                    M.entry(indexInArr(k - 1, l , false, 0, points_in_row),indexInArr(k, l, false, 0, points_in_row)) = phi_A_phi_C + phi_A_phi_B;
                  }
                else {
                    std::cout<<" NO W:"<<k<<" "<<l<<std::endl;
                    no_w++;
                }
            }
        }
        //TODO: Nullbedingung
        //print_to_file(A,"A_j");
        std::cout << "Built matrix. Going to invert..." <<points_in_row<< std::endl;
        // print_to_file(M, "M");
        //print_to_file(A, "A");
        //M.invert();
        std::cout << "NO N:"<< no_n<<std::endl;
        std::cout << "NO NO:"<< no_no<<std::endl;
        std::cout << "NO O:"<< no_o<<std::endl;
        std::cout << "NO S:"<< no_s<<std::endl;
        std::cout << "NO SW:"<< no_sw<<std::endl;
        std::cout << "NO W:"<< no_w<<std::endl;
        M.compress();
        //print_to_file(M, "M_jA");
        std::cout << "Done." << std::endl;
        //print_to_file(M,"MA");
        // M_2.compress();
        map_M[j] = M; //save for later use
        return M;
    };
    auto cEV = [](SM m,size_t j){
            return 1.8/calcEigenvalues<SM,ctype>(m,j);
        };
    std::function<SM(size_t)> A_j = [&](size_t j) {
        auto a = A_h(j);
        std::cout<<"Calced:"<<j<<" AJ"<<std::endl;
        std::cout<<Dune::countNonZeros(a)<<std::endl;
        auto m = M_h(j);
        std::cout<<Dune::countNonZeros(m)<<std::endl;
        m *= (double)timesteps;
        a+=m;
        //a.compress();
        std::cout<<Dune::countNonZeros(a)<<std::endl;
        int x;
   //     std::cin>>x;
        return a;
    };
    for (size_t i = 0; i < timesteps; i++)
    {
        //calculate b_j
        IV b_j (dim);
        M_h(grid_depth).mv(results_over_time->at(i),b_j);
        b_j *= (double)timesteps;
        //std::ofstream d("datei.txt");
        //Dune::printvector(d, b_j, "xk_j", "", 1, 24, 4);
        /* for (size_t j = 0; j < dim ; j++)
        {
            //calculate  integral of right handside, is zero, because we set f=0
            b_j[j] = 0;
        }*/
        auto nextStep = MGM<IV, IV, SM, CGM,double>(grid_depth, u_0, b_j, A_j, &restrictionMatrix<CGM>, cEV, nu_1, nu_2,2);
        //auto directSolve = MGM<IV, IV, SM, CGM,double>(grid_depth, u_0, b_j, A_j, &restrictionMatrix<CGM>, cEV, nu_1, nu_2,6);
        //std::cout<<"Difference:"<<(nextStep-directSolve).two_norm()<<std::endl;
        int x;
        //std::cin >>x;
        //std::ofstream datei("res.txt");
        //Dune::printvector(datei, nextStep, "res", "", 1, 24, 4);
        callback(nextStep);
        results_over_time->push_back(nextStep);
    }
}
void testRestrictionMatrix()
{
    typedef Dune::DynamicMatrix<double> RM;
    auto mat = restrictionMatrix<RM>(2);
    typedef Dune::DynamicVector<ctype> LV;
    std::cout << mat.M() << "x" << mat.N() << std::endl;
    LV test(4, 0);
    LV test2(9, 0);
    LV test3(4, 0);
    test[0] = 1;
    //test[5]=1;
    mat.mtv(test, test2);
    mat.mv(test2, test3);
    //std::ofstream datei("mat.txt");
    //Dune::printmatrix(datei, mat, "R2_1", "", 24, 4);
    //Dune::printvector(datei, test, "test", "", 1, 24, 4);
    //Dune::printvector(datei, test2, "test2", "", 1, 24, 4);
    //Dune::printvector(datei, test3, "test3", "", 1, 24, 12);
}
void testVectorAccess()
{
    const size_t W = pow(2,5);
    typedef Dune::DynamicVector<double> DV;
    DV daten((W + 1) * (W + 1));
    for (size_t i = 0; i < W; i++)
    {
        for (size_t j = 0; j < W; j++)
        {
            //for (size_t l = 0; l < 3; l++)
            {
                accessV2D<DV, double>(daten, i, j, true, 2, W + 1) = 3;
                accessV2D<DV, double>(daten, i, j, false, 2, W + 1) = 3;
            }
        }
    }
    //grid definition
    typedef Dune::UGGrid<dim> GridType;
    typedef Dune::FieldVector<ctype, 2> V2;
    V2 ll, ur;
    ur[1] = 1;
    ur[0] = 1;
    std::array<unsigned, 2> ar, ar2;
    ar[0] = W;
    ar[1] = W;
    ar2[0] = W * 2;
    ar2[1] = W * 2;
    typedef Dune::DynamicMatrix<double> RM;
    auto mat = restrictionMatrix<RM>(6);
    DV daten2((2 * W + 1) * (2 * W + 1));
    mat.mtv(daten, daten2);
    auto grid_ptr = Dune::StructuredGridFactory<GridType>::createSimplexGrid(ll, ur, ar);
    GridType &grid0 = *grid_ptr;
    auto grid_ptr1 = Dune::StructuredGridFactory<GridType>::createSimplexGrid(ll, ur, ar2);
    GridType &grid1 = *grid_ptr1;

    writeVTKFile(grid0.leafGridView(), daten, "grid_coarser");
    writeVTKFile(grid1.leafGridView(), daten2, "grid_finer");
    auto daten1 = daten;
    mat.mv(daten2, daten1);
    writeVTKFile(grid0.leafGridView(), daten1, "grid_corarser2");
    //std::ofstream datei("erg_daten.txt");
    //Dune::printvector(datei, daten, "daten", "", 1, 24, 4);
}
int main(int argc, char **argv)
{
    Dune::MPIHelper::instance(argc, argv);
    try
    {
        //testRestrictionMatrix();
        testVectorAccess();
        typedef Dune::UGGrid<dim> GridType;
        typedef Dune::FieldVector<ctype, 2> V2;
        V2 ll, ur;
        ur[1] = 1;
        ur[0] = 1;
        std::array<unsigned, 2> ar;
        ar[0] = pow(2, refinement_level);
        ar[1] = pow(2, refinement_level);
        auto grid_ptr = Dune::StructuredGridFactory<GridType>::createSimplexGrid(ll, ur, ar);
        GridType &grid0 = *grid_ptr;

        //refine -- not needed because of using alugrid
        /*for(int i=0;i<refinement_level;i++){
            grid0.globalRefine(1);
        }*/
        // Define Pk basis type
        const size_t k = 1;
        typedef GridType::ctype DF;
        typedef double RF;
        std::vector<Dune::DynamicVector<double>> res;
        int i = 0;
        auto callb = [&](Dune::DynamicVector<double> &erg) {
            std::stringstream text;
            text << i++;
           #ifdef BORDER_WARNING
            for(int k=0; k< pow(2, refinement_level)+1;k++ ){
                if(erg[indexInArr(k,0,false,0, pow(2, refinement_level)+1)]!=0.0){
                    std::cerr<<"Warn: Border  Condition (k,0) not met."<<std::endl;
                }
                if(erg[indexInArr(k,pow(2, refinement_level),false,0, pow(2, refinement_level)+1)]!=0.0){
                    std::cerr<<"Warn: Border  Condition ("<<k<<",max) not met."<<erg[indexInArr(k,pow(2, refinement_level),false,0, pow(2, refinement_level)+1)]<<std::endl;
                }
                if(erg[indexInArr(pow(2, refinement_level),k,false,0, pow(2, refinement_level)+1)]!=0.0){
                    std::cerr<<"Warn: Border  Condition (max,"<<k<<") not met:"<<erg[indexInArr(pow(2, refinement_level),k,false,0, pow(2, refinement_level)+1)]<<std::endl;
                }
                if(erg[indexInArr(0,k,false,0, pow(2, refinement_level)+1)]!=0.0){
                    std::cerr<<"Warn: Border  Condition (0,k) not met."<<erg[indexInArr(0,k,false,0, pow(2, refinement_level)+1)]<<std::endl;
                }
            }
            #endif
            writeVTKFile(grid0.leafGridView(), erg, "ref_8_cool_b_0_" + text.str());
            return 0;
        };
        MGM_driver<double>(1000, refinement_level, 50, 50, &res, callb);

        //std::ofstream datei("erg.txt");
        for (int i = 0; i < res.size(); i++)
        {
            std::stringstream text;
            text << i;
            //Dune::printvector(datei, res[i], "erg" + text.str(), "", 1, 24, 4);
            //writeVTKFile(grid0.leafGridView(), res[i], "test" + text.str());
            i++;
        }
    }
    catch (Dune::Exception &e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    catch (std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    return 0;
}