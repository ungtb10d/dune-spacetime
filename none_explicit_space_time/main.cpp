//This follows the tutorial by Oliver Sander (see Introduction to DUNE)
#include "config.h"
#include <dune/common/parallel/mpihelper.hh>

#define HAVE_UG 1
#define HAVE_VIENNACL 1
#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/matrix.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/grid/yaspgrid.hh>
#include "multigrid.hh"
#include "helpers.hh"
#include <dune/functions/functionspacebases/boundarydofs.hh>
#include "assembler.hh"
#include "mgrid.hh"
#include "boundary.hh"
#include "problem.hh"
#include <dune/istl/umfpack.hh>
#include "smoothers.hh"
using Dune::PDELab::Backend::native;
template<typename M,typename VT>
void directSolve(M &A_j,VT &xk_j,  VT b_j){
    typedef Dune::PDELab::Backend::Native<M> NM;
    typedef Dune::PDELab::Backend::Native<VT> NVT;
    typedef Dune::MatrixAdapter<NM, NVT, NVT> LinearOperator;
    Dune::InverseOperatorResult result;
            /*LinearOperator op(native(A_j));
            typedef Dune::SeqILU<NM, NVT, NVT> Preconditioner;
            Preconditioner prec(native(A_j), 1.8);

            const double reduction = 1e-9;
            const int max_iterations = 5000;
            const int verbose = 1;

            typedef Dune::CGSolver<NVT> Solver;
            Solver solver(op, prec, reduction, max_iterations, verbose);
            solver.apply(native(xk_j), native(b_j), result);*/
            Dune::UMFPack<NM> umf(native(A_j),0);
            umf.apply(native(xk_j),native(b_j),result);//TODO(henrik):Warning
}
int main(int argc, char **argv)
{
    Dune::MPIHelper::instance(argc, argv);
    try
    {
        const int dim = 2;
        constexpr unsigned int degree = 1;
        const size_t count = 1000;
        const double h = 0.001;
        typedef double NumberType;
        typedef Dune::UGGrid<dim> GridType;
        typedef Dune::FieldVector<double, 2> V2;
        V2 ll, ur;
        ur[1] = 1;
        ur[0] = 1;
        std::array<unsigned, 2> ar;
        const size_t W = 4;
        ar[0] = W;
        ar[1] = W;
        //auto grid = Dune::StructuredGridFactory<GridType>::createSimplexGrid(ll, ur, ar);
        std ::shared_ptr<GridType> grid(Dune::GmshReader<GridType>::read("l-shape.msh"));
        grid->globalRefine(6);
        int J = grid->maxLevel();
        typedef GridType::LeafGridView GridView;
        GridView gridView = grid->leafGridView();
        auto sourceTerm = [](const Dune::FieldVector<double, dim> &x) {
            if (std::abs(x[0] - 0.5) < 0.25 && std::abs(x[1] - 0.5) < 0.25)
                return 1.0;
            return 0.0;
        };
        // assembleHeatEquationProblemForMethodOfLinesWithoutInvert(basis, matrix, massMatrix, rhs, sourceTerm, 0.001);
        // make problem parameters
        BCTypeParam bctype;
        constexpr Dune::GeometryType::BasicType elemtype = Dune::GeometryType::simplex;
        constexpr Dune::PDELab::MeshType meshtype = Dune::PDELab::MeshType::conforming;
        typedef CGSpaceHierarchy<GridType, NumberType, degree, BCTypeParam, elemtype, meshtype> FS;
        // make a degree of freedom vector on fine grid and initialize it with a function
        // make a degree of freedom vector on fine grid and initialize it with a function

        typedef FS::DOF V;
        FS fs(*grid, bctype);
        fs.assembleAllConstraints(bctype);
        V x(fs.getGFS(J), 0.0);
        //auto flambda = [](const auto &x) { return Dune::FieldVector<double, 1>(0.0); };
        auto g = Dune::PDELab::
            makeGridFunctionFromCallable(gridView, sourceTerm);
        Dune::PDELab::interpolate(g, fs.getGFS(J), x);
        Dune::PDELab::set_constrained_dofs(fs.getCC(J),0.0,x);
        std::cout << "NDOF=" << fs.getGFS(J).globalSize() << std::endl;
        auto flambda = [](const auto &x) { return Dune::FieldVector<double, 1>(0.0); };
        auto f = Dune::PDELab::
            makeGridFunctionFromCallable(gridView, flambda);
        typedef HeatEquationMethodOfLines<decltype(f), FS::FEM> LOP;
        LOP lop(f, fs.getFEM(0).find(*gridView.template begin<0>()),h);
        typedef HeatEquationMethodOfLinesMass<decltype(f), FS::FEM> LOPMass;
        LOPMass lopMass(f, fs.getFEM(0).find(*gridView.template begin<0>()),h);
        typedef HeatEquationProblemAssemblerHierarchy<FS, LOP> ASSEMBLER;

        typedef MatrixHierarchy<ASSEMBLER> MH; //Matrix hierarchy
        typedef HeatEquationProblemAssemblerHierarchy<FS, LOPMass> ASSEMBLERMASS;

        typedef MatrixHierarchy<ASSEMBLERMASS> MHMass; //Matrix hierarchy
        ASSEMBLERMASS asmass(fs,lopMass,4*dim);
        ASSEMBLER as(fs, lop, 4 * dim);

        MH A_factory(as);
        MHMass M(asmass);
        typedef VectorHierarchy<FS> VH;
        VH dummy(fs);
        for (int j = 0; j <= J; j++)
            dummy[j] = 0.0;
        as.jacobian(dummy, A_factory);

        VH dummy2(fs);
        for (int j = 0; j <= J; j++)
            dummy2[j] = 0.0;
        asmass.jacobian(dummy2, M);
        // set up and assemble right hand side w.r.t. l(v)-a(u_g,v)
        V d(fs.getGFS(J), 0.0);
        as.getGO(J).residual(x, d);
        //RichardsonIterationSmoother<FS,V> smoother(fs,50);
        //RichardsonAdaptiveIterationSmootherCL<FS,V> postsmoother(fs,1e-6,500);
        //MultiGridMethod<FS, MH, V,RichardsonAdaptiveIterationSmootherCL<FS,V>, RichardsonAdaptiveIterationSmootherCL<FS,V>, double> mMethod(fs, A_factory, postsmoother,postsmoother);
        typedef Dune::SeqJac<Dune::PDELab::Backend::Native<MH::Matrix>,Dune::PDELab::Backend::Native<V>,Dune::PDELab::Backend::Native<V> > SeqJac;
        SmootherProxy<SeqJac,Dune::PDELab::Backend::Native<MH::Matrix> > smoother([](const Dune::PDELab::Backend::Native<MH::Matrix> &m,size_t j){
            return SeqJac(m,15,.5);
        });
        MultiGridMethod<FS, MH, V,SmootherProxy<SeqJac,Dune::PDELab::Backend::Native<MH::Matrix> > , SmootherProxy<SeqJac,Dune::PDELab::Backend::Native<MH::Matrix> > , double> mMethod(fs, A_factory, smoother,smoother);
        V v(fs.getGFS(J), 0.0);
        auto test= *v.storage();
        auto m = M.getMatrix(J);
        using  Dune::PDELab::Backend::native;
        //test direct solve
        std::vector<V> resultsOverTime;
        resultsOverTime.push_back(x);
        for (size_t i = 0; i < count; i++)
        {
            /* direct solve */
            auto b_j2 = resultsOverTime.at(i);
            V v(fs.getGFS(J),0.0);
            V v2(fs.getGFS(J),0.0);
            V x2(fs.getGFS(J),0.0);
            native(m).mv(native(b_j2),native(x2));
            double start = std::clock();
            mMethod.apply(v, x2);
            double end = std::clock();
            //std::cout<<"Directsolve"<<std::endl;
            directSolve(A_factory[J],v2,x2);
            double enddirect = std::clock();
            std::cout<<"Took"<<end-start<<" vs. " <<enddirect-end<<std::endl;
            native(v2) -= native(v);
            std::cout<<"Difference:"<<(native(v2)).two_norm()<<std::endl;
            resultsOverTime.push_back(v);
            Dune::SubsamplingVTKWriter<FS::GV> vtkWriter(fs.getGFS(J).gridView(), Dune::refinementLevels(degree - 1));
            typename FS::DGF xdgf(fs.getGFS(J), b_j2);
            std::stringstream str;
            str << i;
            auto data = std::make_shared<typename FS::VTKF>(xdgf, "x_h");
            vtkWriter.addVertexData(data);
            vtkWriter.write("result_mgrid_50_adaptive_" + str.str());
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
    }
}