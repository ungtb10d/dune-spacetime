#ifndef SMOOTHERS_HH
#define SMOOTHERS_HH
#include <map>
#include <dune/pdelab.hh>

#include <dune/istl/eigenvalue/poweriteration.hh>
template<class GridHierarchy, class VectorType>
class RichardsonIterationSmoother
{
private:
    size_t nu;
    const GridHierarchy & gh;
    std::map<size_t, double> map_eigen;
    typedef Dune::PDELab::Backend::Native<VectorType> NVT;
    template <typename M>
    double calcEigenvalues(M matrix, size_t j)
    {
        if (map_eigen.count(j) > 0)
        { //return early if we already have the stiffness Matrix calculated
            return map_eigen.at(j);
        }
        auto result = Dune::PowerIteration_Algorithms<M, NVT>(matrix, 20000, 2);
        double dominant;
        VectorType ev(gh.getGFS(j), 1.0);
        //1e-1 exppecting huge eigenvalues
        result.applyPowerIteration(3e-3, Dune::PDELab::Backend::native(ev), dominant);
        std::cout << dominant << std::endl;
        // std::cin>> dominant;
        map_eigen[j] = dominant;
        return dominant;
    }
public:
RichardsonIterationSmoother(const GridHierarchy &_gh,size_t _nu):nu(_nu),gh(_gh){}
    template <class V, class M>
    void apply(V &xk_j, V& b_j,M A_j,size_t j)
    {
        using Dune::PDELab::Backend::native;
        auto omega = 1.8 / calcEigenvalues(native(A_j),j);
        for (size_t k = 0; k < nu; k++)
        {
            auto def = b_j; //Be sure to copy here
            native(A_j).mmv(native(xk_j), native(def));
            def *= omega;
            if(k%10==0){
                std::cout<<"||def||_2:"<<def.two_norm()<<std::endl;
            }
            xk_j += (def); //richardson iteration pre smoothing
        }
    }
};
template<class GridHierarchy, class VectorType>
class RichardsonAdaptiveIterationSmoother
{
private:
    const GridHierarchy & gh;
    std::map<size_t, double> map_eigen;
protected:
    size_t nu_max;
    double eps;
    typedef Dune::PDELab::Backend::Native<VectorType> NVT;
    template <typename M>
    double calcEigenvalues(M matrix, size_t j)
    {
        if (map_eigen.count(j) > 0)
        { //return early if we already have the stiffness Matrix calculated
            return map_eigen.at(j);
        }
        auto result = Dune::PowerIteration_Algorithms<M, NVT>(matrix, 20000, 2);
        double dominant;
        VectorType ev(gh.getGFS(j), 1.0);
        //1e-1 exppecting huge eigenvalues
        result.applyPowerIteration(3e-3, Dune::PDELab::Backend::native(ev), dominant);
        std::cout << dominant << std::endl;
        // std::cin>> dominant;
        map_eigen[j] = dominant;
        return dominant;
    }
public:
RichardsonAdaptiveIterationSmoother(const GridHierarchy &_gh,double _eps, size_t _nu_max):eps(_eps),nu_max(_nu_max),gh(_gh){}
    template <class V, class M>
     void apply(V &xk_j, V& b_j,M A_j,size_t j)
    {
        using Dune::PDELab::Backend::native;
        auto omega = 1.9 / calcEigenvalues(native(A_j),j);
        double error = INFINITY;
        for (size_t k = 0; (k < nu_max); k++)
        {
            auto def = b_j; //Be sure to copy here
            native(A_j).mmv(native(xk_j), native(def));
            def *= omega;
            error = def.two_norm();
            xk_j += (def); //richardson iteration pre smoothing
            if(error<=eps){
                std::cout<<"Took "<<k<<" iterations."<<std::endl;
                std::cout<<"||def||_2:"<<def.two_norm()<<std::endl;
                break;
            }
        }
    }
};
#ifdef HAVE_VIENNACL
#include <viennacl/matrix.hpp>
#include <viennacl/compressed_matrix.hpp>
#include <viennacl/linalg/matrix_operations.hpp>
#include <viennacl/linalg/vector_operations.hpp>
#include <viennacl/linalg/norm_2.hpp>
template<class GridHierarchy, class VectorType>
class RichardsonAdaptiveIterationSmootherCL{
    size_t nu_max;
    const GridHierarchy & gh;
    double eps;
     std::map<size_t, double> map_eigen;
    std::map<size_t, viennacl::compressed_matrix<double> > map_matrix;
    typedef Dune::PDELab::Backend::Native<VectorType> NVT;
     template<class M>
    double calcEigenvalues(M matrix, size_t j)
    {
        if (map_eigen.count(j) > 0)
        { //return early if we already have the stiffness Matrix calculated
            return map_eigen.at(j);
        }
        //viennacl::linalg::eig(A, ptag, eigenvector)
        auto result = Dune::PowerIteration_Algorithms<M, NVT>(matrix, 20000, 2);
        double dominant;
        VectorType ev(gh.getGFS(j), 1.0);
        //1e-1 exppecting huge eigenvalues
        result.applyPowerIteration(3e-3, Dune::PDELab::Backend::native(ev), dominant);
        std::cout << dominant << std::endl;
        // std::cin>> dominant;
        map_eigen[j] = dominant;
        return dominant;
    }
    template< class M>
    viennacl::compressed_matrix<double>& getMatrix(M &native_m,size_t j){
        if (map_matrix.count(j) > 0)
        { //return early if we already have the stiffness Matrix calculated
            return map_matrix.at(j);
        }
        viennacl::compressed_matrix<double>  vcl_A(native_m.M(), native_m.N());
        std::vector< std::map< unsigned int, double> > unmap_a(native_m.M());
        for(auto row= native_m.begin();row!=native_m.end();row++){
            for(auto col = row->begin();col!= row->end();col++){
                unmap_a[row.index()][col.index()]= *col;
            }
        }
        //copy to OpenCL device:
        copy(unmap_a, vcl_A);
        map_matrix[j]= vcl_A;
        return map_matrix[j];
    }
    public:
    RichardsonAdaptiveIterationSmootherCL(const GridHierarchy &_gh,double _eps, size_t _nu_max):eps(_eps),nu_max(_nu_max),gh(_gh){}
    template <class V, class M>
     void apply(V &xk_j, V& b_j,M A_j,size_t j)
    {
        using Dune::PDELab::Backend::native;
        viennacl::vector<double> v_x(native(xk_j).N()),v_b(native(b_j).N()),def(native(b_j).N());
        copy(native(xk_j).begin(), native(xk_j).end(), v_x.begin());
        copy(native(b_j).begin(), native(b_j).end(), v_b.begin());
        auto omega = 1.9 / calcEigenvalues(native(A_j),j);
        viennacl::compressed_matrix<double>&vcl_A = getMatrix(native(A_j),j);
        double error = INFINITY;
        std::cout<<"Running "<<nu_max<<" times..."<<std::endl;
        for (size_t k = 0; (k < nu_max); k++)
        {
            def = viennacl::linalg::prod(vcl_A,v_x);
            if(viennacl::linalg::norm_2(def)<eps){
                break;
            }
            v_x = v_x+omega*(v_b-def);//need to store
        }
        std::cout<<"...Done."<<std::endl;
        copy(v_x.begin(), v_x.end(),native(xk_j).begin() );
    }
};
#endif
//w (relaxation ) in (0,2)
template<class Smoother, class MT>
class SmootherProxy{
    std::map<size_t,Smoother> sms;
     std::function<Smoother(const MT &matrix,int i,int j)> factory;
    public:
    SmootherProxy( std::function<Smoother(const MT &matrix,int i,int j)> _factory):factory(_factory){

    }
    template <class V, class M>
     void apply(V &xk_j, V& b_j,M A_j,size_t time_i,size_t j)
    {
        using Dune::PDELab::Backend::native;
        /*if(sms.find(j)==0){
            sms[j]= ;
        }*/
        factory(native(A_j),time_i,j).apply(native(xk_j),native(b_j));
    }
};
#endif